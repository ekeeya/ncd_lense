import React from 'react';
import { Text, View } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
const ActivityLoader = ({loading}) => (
    <AwesomeAlert
          show={loading}
          showProgress={true}
          title=""
          message='Please Wait...'
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={false}
          cancelText="No, cancel"
          confirmText="close"
          confirmButtonColor="#dddd"
        />
);

export default ActivityLoader;
