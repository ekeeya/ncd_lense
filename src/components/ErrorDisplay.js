import React, { Component } from 'react';
import {  View, Text } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import { Icon } from 'native-base'
class ErrorDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showAlert:true
    };
  }

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
    this.props.reset()
  };
  renderAlert=()=>{
    return(
    <View style={{ alignItems: 'center', justifyContent: 'center', }}>
        <Icon type={"MaterialIcons"} name={"error"} style={{ color:'red',fontSize: 50}}/>
        <View style={{ alignItems:'center',justifyContent:'space-evenly',width:200, alignContent:'space-around'}} >
        <Text style={{alignSelf:'center'}}>{this.props.errorMsg}</Text>
        </View>
    </View>)
  } 

  render() {
    return (
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          customView={this.renderAlert()}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          cancelText="No, cancel"
          confirmText="CLOSE"
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
    );
  }
}


export default ErrorDisplay;