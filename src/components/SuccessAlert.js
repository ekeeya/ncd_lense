import React, { Component } from 'react';
import {  View, Text } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import { Icon } from 'native-base'
class SuccessAlert extends Component {
  constructor(props) {
    super(props);
    this.state = {
        showAlert:true
    };
  }

  hideAlert = () => {
    this.setState({
      showAlert: false
    });
    this.props.reset()
  };
  renderAlert=()=>{
    return(
    <View style={{ alignItems: 'center', justifyContent: 'center', }}>
        <Icon type={"AntDesign"} name={"checkcircle"} style={{ color:'green',fontSize: 50}}/>
        <View style={{ alignItems:'center',justifyContent:'space-evenly',width:200, alignContent:'space-around'}} >
        <Text style={{alignSelf:'center',color:'green'}}>{this.props.successMsg}</Text>
        </View>
    </View>)
  } 

  render() {
    return (
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          customView={this.renderAlert()}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          cancelText="No, cancel"
          confirmText="CLOSE"
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
    );
  }
}


export default SuccessAlert