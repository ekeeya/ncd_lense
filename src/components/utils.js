import DialogProgress from 'react-native-dialog-progress';

const showDialogProgress = msg => {
  const options = {
    message: msg,
    isCancelable: true,
  };
  DialogProgress.show(options);
};

const hideDialogProgress = _ => {
  DialogProgress.hide();
};

export {showDialogProgress, hideDialogProgress};
