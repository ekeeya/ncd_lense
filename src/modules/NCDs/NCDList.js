import React from 'react';
import {
  StyleSheet,
  View,
  Platform,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import { colors, fonts } from '../../styles';
import { FAB } from 'react-native-paper';
import CompleteFlatList from 'react-native-complete-flatlist';


export default class NCDList extends React.Component {
  
    _openNCD = ncd => {
        this.props.navigation.navigate({
          routeName: 'NcdDisease',
          params: { ...ncd },
        });
      };
    
      renderFAB = () => {
        return(
          <FAB
          style={styles.fab}
          large
          icon="refresh"
          onPress={() => this.props.fetchNCDs()}
        />
        );
      }
    
    UNSAFE_componentWillMount =()=>{
        this.props.fetchNCDs()
    }
  renderNCDs = (healthRec, index) => {
    const info = healthRec.cleanData ? healthRec.cleanData : healthRec
    const item = healthRec.cleanData ? healthRec.cleanData.cleanData : info.item
    const item1 = item.cleanData ? item.cleanData : item
    return(
        <TouchableOpacity
        key={1}
        style={styles.itemThreeContainer}
        onPress={() => this._openNCD(item1)}
        >
        <View style={styles.itemThreeSubContainer}>
            <Image source={{ uri: item1.image_url }} style={styles.itemThreeImage} /> 
            <View style={styles.itemThreeContent}>
            <Text style={styles.itemThreeBrand}>{item1.disease_title}</Text>
          
            <View style={styles.itemThreeMetaContainer}>
              <Text style={styles.ncdDescriptionText}>{`${(item1.definition).substring(0,142)}.........`}</Text>
            </View>
            </View>
        </View>
        </TouchableOpacity>
    )
  }
   
 
  render() {
    const healthRec = this.props.ncdData
    return (
      <View style={styles.container}>
        {this.props.isLoading ?
        <ActivityIndicator size='large'/>
        :
        <CompleteFlatList
        searchKey={['disease_title']}
        highlightColor="red"
        placeholder="Search Disease"
        data={healthRec}
        ref={c => this.completeFlatList = c}
        renderSeparator={null}
        renderEmptyRow ={()=>{}}
        renderItem={this.renderNCDs}
    />
        }
        {this.renderFAB()}
      </View>
    );
  }
  renderEmptyBox = ()=>{
    return(
      <View style={{justifyContent:'center',alignItems:'center',alignContent:'center'}}>
         <Text>Nothing to show</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  tabsContainer: {
    alignSelf: 'stretch',
    marginTop: 30,
  },
  itemOneContainer: {
    flex: 1,
    width: Dimensions.get('window').width / 2 - 25,
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: colors.primary,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    padding: 8,
  },
  itemOneImageContainer: {
    borderRadius: 3,
    overflow: 'hidden',
  },
  itemOneImage: {
    height: 150,
    width: Dimensions.get('window').width / 2 - 45,
    borderRadius: 10,
  },
  itemOneTitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  itemOneSubTitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 13,
    color: '#B2B2B2',
    marginVertical: 3,
  },
  itemOnePrice: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  itemOneRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  itemOneContent: {
    marginTop: 5,
    marginBottom: 10,
  },
  itemTwoContainer: {
    paddingBottom: 10,
    backgroundColor: colors.whiteTwo,
    marginVertical: 5,
  },
  itemTwoContent: {
    padding: 20,
    position: 'relative',
    marginHorizontal: Platform.OS === 'ios' ? -15 : 0,
    height: 150,
  },
  itemTwoTitle: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 20,
  },
  itemTwoSubTitle: {
    color: colors.white,
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
    marginVertical: 5,
  },
  itemTwoPrice: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 10,
  },
  itemTwoImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  itemTwoOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
  },
  itemThreeContainer: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    shadowColor: colors.primary,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginVertical: 8,
  },
  itemThreeSubContainer: {
    flexDirection: 'row',
    paddingVertical: 10,
  },
  itemThreeImage: {
    height: 100,
    width: 100,
    borderRadius: 10,
  },
  itemThreeContent: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: 'space-between',
  },
  itemThreeBrand: {
    fontFamily: fonts.primaryRegular,
    fontSize: 18,
    color: '#617ae1',
  },
  ncdDescriptionText: {
    fontFamily: fonts.primaryRegular,
    fontSize: 13,
    color: '#5F5F5F',
  },
  itemThreeTitle: {
    fontFamily: fonts.primaryBold,
    fontSize: 14,
    color: '#5F5F5F',
  },
  itemThreeSubtitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 16,
    marginLeft:10,
    color: '#a4a4a4',
  },
  itemThreeMetaContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemThreePrice: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
    color: '#5f5f5f',
    textAlign: 'right',
  },
  itemThreeHr: {
    flex: 1,
    height: 1,
    backgroundColor: '#e3e3e3',
    marginRight: -15,
  },
  badge: {
    backgroundColor: colors.labelTwo,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    backgroundColor: '#004ecb',
    right: 0,
    bottom: 0,
  },
});
