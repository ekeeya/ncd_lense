import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import { getNCData } from '../../redux/actions/getNCDs';
import NCDList from './NCDList';



function mapStateToProps(state){
    return{
        isLoading: state.getNCDs.isLoading,
        ncdData: state.getNCDs.ncdData,
        ncdError: state.getNCDs.ncdError
    }
}
function mapDispatchToProps(dispatch){
    return{
        fetchNCDs: ()=>dispatch(getNCData())
    }
}
export default compose(connect(mapStateToProps,mapDispatchToProps))(NCDList);
