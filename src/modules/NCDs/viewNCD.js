import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  Dimensions,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {Icon} from 'native-base';

import { colors } from '../../styles';
import { connect } from 'react-redux'
import { getNCDFacts } from '../../redux/actions/fetchAllNCDFacts'
import { Dropdown, Button } from '../../components';
import { Text, Title, Caption } from '../../components/StyledText';
import AppLink from 'react-native-app-link';
import AwesomeAlert from 'react-native-awesome-alerts';
const { width, height } = Dimensions.get('window')
const options =  [
  'DESCRIPTION',
  'CAUSES',
  'SYMPTOMS',
  'INVESTIGATION',
  'PROGNOSIS',
  'PREVENTION',
]
class NcdDetailScreen extends React.Component {
    constructor(props){
      super(props)
      this.state={
        optionVal:-1,
        optionItem:"DESCRIPTION",
        showFacts:false
      }
    }
  

    hideAlert = ()=>{
      this.setState({
        showFacts:false
      })
    }

    UNSAFE_componentWillMount = ()=>{
      console.log(this.props)
      this.props.fetchNCDFacts()
    }
    renderWhoRecommendations = () => {
      return (
        <View style={{ alignItems: 'center', height:  height / 1.5, marginLeft: 10, marginRight: 10 }}>
          
          <Caption bold color={colors.yellow} size={15}>
            FACTS ON NCDs
          </Caption>
          <ScrollView style={{ marginTop: 5 }}>
          <Title style={styles.p} size={13} color={`#000000`}>
           {this.props.ncdFacts ? this.props.ncdFacts.facts_on_ncds : `No data to display`}
          </Title>
          </ScrollView>
          <Caption bold color={colors.yellow} size={15} style={{ marginTop: 5 }}>
            RISK FACTORS
          </Caption>
          <ScrollView style={{ marginTop: 5 }}>
          <Title style={styles.p} size={13} color={`#000000`}>
          {this.props.ncdFacts ? this.props.ncdFacts.risk_factors : `No data to display`}
          </Title>
          </ScrollView>
          <Caption bold color={colors.yellow} size={15} style={{ marginTop: 5,alignSelf:'center' }}>
            DIET
          </Caption>
          <ScrollView style={{ marginTop: 5, minHeight:50 }}>
          <Title style={styles.p} size={13} color={`#000000`}>
          {this.props.ncdFacts ? this.props.ncdFacts.diet : `No data to display`}
          </Title>
          </ScrollView>
          <Caption bold color={colors.yellow} size={15} style={{ marginTop: 5,alignSelf:'center' }}>
            PHYSICAL ACTIVENESS
          </Caption>
          <ScrollView style={{ marginTop: 5, minHeight:50 }}>
          <Title style={styles.p} size={13} color={`#000000`}>
          {this.props.ncdFacts ? this.props.ncdFacts.physical_activeness : `No data to display`}
          </Title>
          </ScrollView>
        </View>
      )
    }

    appLinker = (url, appName, playStoreId, ) => {
      AppLink.maybeOpenURL(url, { appName, playStoreId }).then(() => {
        // do stuff
      })
        .catch((err) => {
          // handle error
        });
    }

  render(){
  const itemParams = this.props.navigation.state.params;
  
  return (
    <>
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <View
          style={{
            flexDirection: 'column',
            flex: 1,
          }}
        >
          <Text style={{ color: colors.primaryLight,fontSize:24 }}>
            {itemParams.disease_title}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'stretch',
              marginTop: 8,
            }}
          >
          </View>
        </View>
      </View>
      <View style={styles.carouselContainer}>
        <Carousel
          autoplay
          sliderWidth={Dimensions.get('window').width }
          itemWidth={Dimensions.get('window').width}
          renderItem={({ item }) => (
            <Image
              resizeMode="contain"
              style={{ height: 250, width: Dimensions.get('window').width }}
              source={{ uri: item}} 
            />
          )}
          data={[
            itemParams.image_url,
            itemParams.image_url
          ]}
        />
        
      </View>
      <View style={styles.buttonsSection}>
              <View style={{ flex: 2 }}>
                <Button onPress={() => this.setState({showFacts:true})} 
                secondary caption="CLICK TO VIEW NCDs FACTS" rounded 
                style={{width:width*0.8,alignSelf:'center'}} 
                />
              </View>
            </View>
      <View style={styles.bodyContainer}>
        <View style={{ paddingTop: 20 }}>
          <Text color={colors.primaryLight}>Disease Details</Text>
          <View style={styles.row}>
            <View style={styles.sizeDropdownContainer}>
              <Dropdown
                borderColor={colors.grey}
                color={colors.gray}
                
                placeholder={this.state.optionVal ===  -1 ? `Select To Switch Amongest Disease Details` : options[this.state.optionVal]}
                  items={[
                    'DESCRIPTION',
                    'CAUSES',
                    'SYMPTOMS',
                    'INVESTIGATION',
                    'PROGNOSIS',
                    'PREVENTION',
                  ]}
                  onSelect={(index) =>
                  this.setState({optionVal:index})
                  }
              />
            </View>
            
          </View>
        </View>
        
        <View style={styles.description}>
          <Title bold color={colors.lightGray} size={17}>
            {(this.state.optionVal == 0 || this.state.optionVal == -1) ?
            `DESCRIPTION`
            :
            `${options[this.state.optionVal]}`  
            }
          </Title>
          <Text style={styles.p}>
            {
              this.state.optionVal == -1 ?
              itemParams.definition:
              null
            }
            {
            this.state.optionVal == -1 ?
            itemParams.definition
            :
            this.state.optionVal == 0 ?
            itemParams.definition
            :
            this.state.optionVal == 1 ?
            itemParams.causes
              :
            this.state.optionVal == 2 ?
              itemParams.clinical_features
            :
            this.state.optionVal == 3 ?
            itemParams.investigation
            :
            this.state.optionVal == 4 ?
            itemParams.prognosis
            :
            itemParams.prevention
          }
          </Text>
        </View>
        
      </View>
      

      <View style={{ paddingBottom: 50, paddingHorizontal: 15 }}>
        <Title color={colors.lightGray} style={{ marginVertical: 10 }}>
          Share
        </Title>
        <View style={{ flexDirection: 'row' }}>
          <Text color={colors.gray}>Share with a tag</Text>
          <Text color={colors.blue} style={{ marginLeft: 5 }}>
            #ncdLens
          </Text>
        </View>

        <View style={{ flexDirection: 'row', marginTop: 15 }}>
          <Button action bgColor="#4867AD" 
          style={{paddingHorizontal:10}}
          onPress={() => {
            this.appLinker(
              'https://www.facebook.com',
              'Facebook',
              'com.facebook.katana'
            )

          }}>
            <Icon type="FontAwesome" name="facebook" style={{ size: 25, color: "white" }} />
          </Button>
          {/* <Button
            action
            bgColor="#7A3EB1"
            onPress={() => { }}
            style={{ marginHorizontal: 15 }}
          >
            <Icon type="FontAwesome" name="instagram" style={{ size: 25, color: "white" }} />
          </Button> */}
          <Button action bgColor={colors.blue} onPress={() => {
            this.appLinker(
              'https://twitter.com/elkeeya',
              'Twitter',
              'com.twitter.android'
            )
          }}>
            <Icon type="FontAwesome" name="twitter" style={{ size: 25, color: "white" }} />
          </Button>
        </View>
      </View>
    </ScrollView>
    <AwesomeAlert
          show={this.state.showFacts}
          title={`FACTS ABOUT NON-COMMUNICABLE DISEASES`}
          titleStyle={styles.alertTitle}
          showProgress={false}
          customView={this.renderWhoRecommendations()}
          closeOnTouchOutside={true}
          alertContainerStyle={{ width: width }}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={false}
          confirmButtonColor="green"
          cancelButtonColor={colors.secondaryGradientStart}
          cancelText="Close"
          onCancelPressed={() => {
            this.hideAlert();
          }}
        />
    </>
  );
 }
}

function mapStateToProps(state){
  return{
    ncdFacts:state.getNCDFacts.ncdFacts,
    ncdGetError:state.getNCDFacts.ncdGetError
  }
}
function mapDispatchToProps(dispatch){
  return{
      fetchNCDFacts: ()=>dispatch(getNCDFacts())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NcdDetailScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  carouselContainer: {
    marginVertical: 10,
    paddingHorizontal: 15,
  },
  bodyContainer: {
    paddingHorizontal: 15,
  },
  bodyHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  sizeDropdownContainer: {
    flex: 2,
    paddingVertical: 10,
    paddingRight: 5,
    
  },
  quantityDropdownContainer: {
    flex: 1,
    paddingVertical: 10,
    paddingLeft: 5,
  },
  buttonsSection: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  actionButtonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  alertTitle: {
    marginVertical: 5,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
    fontSize: 12,
    fontWeight: 'bold'
  },
  p: {
    marginVertical: 5,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
  },
  description: {
    paddingTop: 10,
    marginVertical: 10,
  },
  
  badge: {
    paddingHorizontal: 10,
    paddingVertical: 3,
    backgroundColor: colors.green,
    position: 'absolute',
    left: 15,
    top: 0,
  },
  recommendationItemTopContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingRight: 5,
  },
  recommendationItemBadge: {
    backgroundColor: colors.secondary,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  recommendationItemRating: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 3,
  },
});
