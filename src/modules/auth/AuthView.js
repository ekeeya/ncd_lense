import React, { Component } from 'react';
import { View, Alert, Text, StatusBar, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native';
import Svg, { Image, Circle, ClipPath } from 'react-native-svg'
import Animated, { Easing } from 'react-native-reanimated';
import { Button, Icon } from 'native-base';
import { TapGestureHandler, State } from 'react-native-gesture-handler'
import ActivityLoader from '../../components/ActivityLoader'
import ErrorDisplay from '../../components/ErrorDisplay'
import SuccessAlert from '../../components/SuccessAlert';
//import { AccessToken, LoginManager, LoginButton } from 'react-native-fbsdk';
import { firebase } from '@react-native-firebase/auth';
import FastImage from 'react-native-fast-image'
import {
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
import { showDialogProgress, hideDialogProgress } from '../../components/utils'


const { Value, event, block, cond, eq, set, Clock, startClock, stopClock, debug, timing,
  clockRunning, interpolate, Extrapolate, concat } = Animated
const bgImage = require('../../../assets/images/workout.jpg')
const { width, height } = Dimensions.get('window')

const logoImage = require('../../../assets/images/logo.png')


function runTiming(clock, value, dest) {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0)
  };

  const config = {
    duration: 1000,
    toValue: new Value(0),
    easing: Easing.inOut(Easing.ease)
  };

  return block([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, value),
      set(state.frameTime, 0),
      set(config.toValue, dest),
      startClock(clock)
    ]),
    timing(clock, state, config),
    cond(state.finished, debug('stop clock', stopClock(clock))),
    state.position
  ]);
}


class AuthScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      lastName: '',
      firstName: '',
      isFieldError: null
    }
    this.buttonOpacity = new Value(1)
    this.onStateChange = event([
      {
        nativeEvent: ({ state }) =>
          block([cond(eq(state, State.END), set(this.buttonOpacity, runTiming(new Clock(), 1, 0)))
          ])
      }
    ])

    this.onCloseState = event([
      {
        nativeEvent: ({ state }) =>
          block([cond(eq(state, State.END), set(this.buttonOpacity, runTiming(new Clock(), 0, 1)))
          ])
      }
    ])

    this.buttonY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [100, 0],
      extrapolate: Extrapolate.CLAMP
    })

    this.bgY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [-height / 2 - 200, 0],
      extrapolate: Extrapolate.CLAMP
    })

    this.textInputZindex = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [1, -1],
      extrapolate: Extrapolate.CLAMP
    })
    this.textInputY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [0, 100],
      extrapolate: Extrapolate.CLAMP
    })
    this.textInputOpacity = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [1, 0],
      extrapolate: Extrapolate.CLAMP
    })
    this.rotateCross = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [180, 360],
      extrapolate: Extrapolate.CLAMP
    })

  }
  renderGoogleButton = () => {
    return (
      <GoogleSigninButton
        style={{ width: 200, height: 50 }}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={() => this.props.authenticateUserWithGoogle()} />
    )
  }
  /* renderFacebookButton = () => {
    return (
      <LoginButton
        onLoginFinished={
          (error, result) => {
            if (error) {
              console.log("login has error: " + result.error);
            } else if (result.isCancelled) {
              console.log("login is cancelled.");
            } else {
              AccessToken.getCurrentAccessToken().then(
                (data) => {
                  console.log("Shit works")
                  console.log(data)
                }
              )
            }
          }
        }
        onLogoutFinished={() => console.log("logout.")} />
    )
  } */
  _onRegister = () => {
    if (this.state.lastName === '') {
      this.setState({ isFieldError: 'Please provide your last name to proceed' })
    }
    else if (this.state.firstName === '') {
      this.setState({ isFieldError: 'Please provide your First name to proceed' })
    }
    else if (this.state.email === '') {
      this.setState({ isFieldError: 'Please provide your email address to proceed' })
    }
    else if (this.state.password.length < 6) {
      this.setState({ isFieldError: `Very weak password, it should be at least 6 characters` })
    }
    else {
      this.props.registerWithEmail(this.state)
    }
  }

  _resetAuth = () => {
    this.props.resetAuth()
  }

  executeFunctions = uid => {
    this.props.checkQuestionier(uid, 1)
    this.props.fecthHeathRecs(uid, 1)
    this.props.fetchlatestRec(uid, 1)
  }

  safeRedirect = (uid,userData)=>{
    console.log(this.props.isNewUser)
    if (this.props.isNewUser) {
      this.props.checkQuestionier(uid, 1)
      console.log("user is new")
      setTimeout(()=>{
        this.props.navigation.navigate('Main', userData)
      },20)
    }
    else  {
      console.log("we have him already")
      this.executeFunctions(uid)
      setTimeout(()=>{
        this.props.navigation.navigate('Main', userData)
      },20)
    }
  }
  
  LoginWithFirebase = () => {
    if (this.state.email === '') {
      this.setState({ isFieldError: 'Please provide your email address to proceed' })
    }
    else if (this.state.password === '') {
      this.setState({ isFieldError: `Please provide a password` })
    }
    else {
      this.props.loginWithEmail(this.state.email, this.state.password)
    }
  }
  checkIfLoggedIn = () => {
    try {
      firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            let userData;
            console.log(user)
            if (user._user.providerData[0].providerId !== 'password') {
              userData = user._user.providerData[0]
              const { uid } = userData
              this.safeRedirect(uid, userData)
            }
            else {
              const { displayName, email, phoneNumber, photoURL, uid } = user._user
              userData = { displayName, email, phoneNumber, photoURL, uid }
              this.safeRedirect(uid, userData)
            }
        }
        else {
          console.log("new user")
        }
      })
    } 
    catch (error) {
      console.log(error)
    }
  }
   componentDidMount() {
    setTimeout(()=>{
      this.checkIfLoggedIn()
    },100)
  } 
   /* componentDidUpdate() {
    setTimeout(()=>{
       if(this.props.isNewUser || ~this.props.isNewUser){
        console.log(this.props.isNewUser)
        this.checkIfLoggedIn()
      } 
    },100)
  }  */

  resetEmailRegistration = () => {
    this.setState({ isFieldError: null })
    this.props.resetEmailReg()
  }
  continueLogin = () => {
    this.props.resetEmailReg()
    this.props.loginWithEmail(this.state.email, this.state.password)
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'flex-end' }}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Animated.View style={{
          ...StyleSheet.absoluteFill, transform: [{
            translateY: this.bgY
          }]
        }}>
          <Svg height={height + 50} width={width}>
            <ClipPath id="clip">
              <Circle
                r={height + 50}
                cx={width / 2}
              />
            </ClipPath>
            <Image
              href={bgImage}
              height={height + 50}
              width={width}
              preserveAspectRatio='xMidYMid slice'
              clipPath='url(#clip)'
            />
          </Svg>
        </Animated.View>
        <View style={{ height: height / 3, justifyContent: 'center' }}>
          <Animated.View style={{
            opacity: this.buttonOpacity,
            borderTopLeftRadius: 200,
            borderTopRightRadius: 200,
            backgroundColor: 'white',
            transform: [{ translateY: this.buttonY }],
            height: height / 2, ...StyleSheet.absoluteFill,
            top: null, justifyContent: 'center'
          }}>
            <View style={{ alignItems: 'center' }}>
              <FastImage
                style={{ width: 100, height: 80 }}
                source={logoImage}
                resizeMode={FastImage.resizeMode.contain}
              />
            </View>

            <TextInput
              placeholder="EMAIL"
              keyboardType='email-address'
              style={styles.textInputSignin}
              placeholderTextColor="black"
              onChangeText={(email) => this.setState({ email })}
            />
            <TextInput
              placeholder="PASSWORD"
              style={styles.textInputSignin}
              secureTextEntry={true}
              placeholderTextColor="black"
              onChangeText={(password) => this.setState({ password })}
            />

            <TouchableOpacity onPress={() => this.LoginWithFirebase()} >
              <View style={styles.buttonSignin}>
                <Text style={styles.buttonText}>SIGN IN</Text>
              </View>
            </TouchableOpacity>
            {this.props.isLoading ? showDialogProgress('Authenticating user, please wait...') : hideDialogProgress()}
            {this.props.isLoggedIn && this.props.navigation.navigate('Main', this.props.user)}
            <TapGestureHandler onHandlerStateChange={this.onStateChange}>
              <Animated.View style={{ alignItems: 'center' }}>
                <Text>OR</Text>
                <Text style={{ ...styles.buttonText, color: '#2E71DC', marginTop: 15 }}>SIGN UP</Text>
              </Animated.View>
            </TapGestureHandler>
          </Animated.View>

          <Animated.View style={{
            zIndex: this.textInputZindex,
            opacity: this.textInputOpacity,
            transform: [{ translateY: this.textInputY }],
            height: height / 2 + 150, ...StyleSheet.absoluteFill,
            top: null, justifyContent: 'center'
          }}>

            <TapGestureHandler onHandlerStateChange={this.onCloseState}>
              <Animated.View style={styles.closeButton}>
                <Animated.Text style={{
                  fontSize: 15, transform: [{
                    rotate: concat(this.rotateCross, 'deg')
                  }]
                }}>X</Animated.Text>
              </Animated.View>
            </TapGestureHandler>
            <View style={{
              flexDirection: 'row',
              alignItems: 'center', alignContent: 'center',
              justifyContent: 'center', paddingVertical: 20, marginTop: 20,
            }}>

              {this.renderGoogleButton()}
              <Button iconLeft full primary
                style={{ height: 40, width: 200, borderRadius: 3, alignSelf: 'center' }}
                onPress={() => this.props.authenticateUserWithFacebook()}
              >
                <Icon
                  type='FontAwesome5'
                  name="facebook-f"
                  size={20}
                  color="white"
                />
                <Text style={{ color: 'white', paddingHorizontal: 10, fontWeight: 'bold' }}>Sign In with Facebook</Text>
              </Button>
            </View>
            <TextInput
              placeholder="First Name"
              style={styles.textInput}
              placeholderTextColor="black"
              onChangeText={(firstName) => this.setState({ firstName })}
            />
            <TextInput
              placeholder="Last Name"
              style={styles.textInput}
              placeholderTextColor="black"
              onChangeText={(lastName) => this.setState({ lastName })}
            />
            <TextInput
              placeholder="Email"
              style={styles.textInput}
              keyboardType='email-address'
              placeholderTextColor="black"
              onChangeText={(email) => this.setState({ email })}
            />
            <TextInput
              placeholder="password"
              style={styles.textInput}
              secureTextEntry={true}
              placeholderTextColor="black"
              onChangeText={(password) => this.setState({ password })}
            />
            <TouchableOpacity
              onPress={() => this._onRegister()}
              style={styles.button}>
              <Text style={{ fontSize: 20, fontWeight: 'bold' }}>SUBMIT</Text>
            </TouchableOpacity>
            {this.props.isRegistering ? showDialogProgress('Registering user, please wait...') : hideDialogProgress()}
            {
              (this.props.regError) &&
              <ErrorDisplay errorMsg={this.props.regError} reset={this.resetEmailRegistration} />
            }
            {
              (this.state.isFieldError) &&
              <ErrorDisplay errorMsg={this.state.isFieldError} reset={this.resetEmailRegistration} />
            }
          </Animated.View>
        </View>
        {
          this.props.error &&
          <ErrorDisplay errorMsg={this.props.error} reset={this._resetAuth} />
        }
        {this.props.isLoading ? showDialogProgress('Authenticating user, please wait...') : hideDialogProgress()}
      </View>
    );
  }
}

export default AuthScreen;

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'white',
    height: 70,
    marginHorizontal: 20,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 1
  },
  buttonSignin: {
    backgroundColor: 'white',
    height: 70,
    marginHorizontal: 45,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 1
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  closeButton: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -20,
    left: width / 2 - 20,
    shadowOffset: { width: 2, height: 2 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    elevation: 1
  },
  textInput: {
    height: 50,
    borderRadius: 25,
    borderWidth: 0.5,
    marginHorizontal: 20,
    paddingLeft: 10,
    marginVertical: 5,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  textInputSignin: {
    height: 50,
    borderRadius: 15,
    borderBottomWidth: 0.5,
    marginHorizontal: 40,
    paddingLeft: 10,
    textAlign: 'center',
    marginVertical: 5,
    borderColor: 'rgba(0,0,0,0.2)'
  },

})
