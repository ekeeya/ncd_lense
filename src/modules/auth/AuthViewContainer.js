// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { signinWithGoogle, signInWithFacebook, signUpWithEmail, signInWithEmail,resetEmailSignup, resetAuthState } from '../../redux/actions/signInWithFirebase';
import { checkQuestionierData } from '../../redux/actions/checkQuestionier';
import { getHealthRecords } from '../../redux/actions/getHealthRecords';
import { fetchUserHealthRec } from '../../redux/actions/latestUserHealthRec'

import AuthView from './AuthView';

function mapStateToProps(state){
    return{
        isLoading:state.signInWithFirebase.isLoading,
        isLoggedIn:state.signInWithFirebase.isLoggedIn,
        error:state.signInWithFirebase.error,
        user:state.signInWithFirebase.user,
        isNewUser:state.signInWithFirebase.isNewUser,
        provider:state.signInWithFirebase.provider,
        isRegistering:state.signUpWithEmail.isRegistering,
        hasRegistered:state.signUpWithEmail.hasRegistered,
        regError:state.signUpWithEmail.regError,
        bioInfo:state.checkQuestionier.bioInfo
    }
}

function mapDispatchToProps(dispatch){
    return{
        authenticateUserWithGoogle:()=>dispatch(signinWithGoogle()),
        authenticateUserWithFacebook:()=>dispatch(signInWithFacebook()),
        checkQuestionier:(userId,isFirebase)=>dispatch(checkQuestionierData(userId,isFirebase)),
        fecthHeathRecs :(userID,isFirebase)=>dispatch(getHealthRecords(userID,isFirebase)),
        fetchlatestRec: (userId,fromFirebase)=>{dispatch(fetchUserHealthRec(userId,fromFirebase))},
        registerWithEmail:(state)=>{dispatch(signUpWithEmail(state))},
        loginWithEmail:(email,password)=>{dispatch(signInWithEmail(email,password))},
        resetEmailReg :()=>dispatch(resetEmailSignup()),
        resetAuth :()=>dispatch(resetAuthState())
    }
}

export default compose(connect(mapStateToProps,mapDispatchToProps))(AuthView);
