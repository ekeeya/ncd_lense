/* eslint-disable class-methods-use-this */
import React from 'react';
import { StyleSheet, View,TouchableOpacity, Text } from 'react-native';
import { Agenda } from 'react-native-calendars';
//import { RadioGroup, GridRow } from '../../components';
import AwesomeAlert from 'react-native-awesome-alerts';
import { FAB,  Portal, Provider } from 'react-native-paper';
import { showDialogProgress, hideDialogProgress } from '../../components/utils'

import { colors, fonts } from '../../styles';

class CalendarScreen extends React.Component {
  constructor(props){
    super(props)
    this.state = { 
      showAlert: false,
      id:'0',
      open:false,
      showSuccess:true
    };
  }

  UNSAFE_componentWillMount =()=>{
    
    this.props.fetchEvents()
  }

  handleItemAction = item =>{
    this.setState({
      showAlert: true,
      id:item.id
    });

  }  
  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  hideAlert = () => {
    this.setState({
      showAlert: false,
      showSuccess:false
    });
  };
  refreshList =() =>{
    this.setState({
      showAlert: false
    },()=>{
      this.props.fetchEvents()
      this.props.hideAlert();
    });
  }
  handleDelete = ()=>{
    this.setState({
      showAlert:false
    },()=>{
      this.props.deleteEvent(this.state.id.toLocaleString())
    })
    
  }
  handleEdit = ()=>{
    this.setState({
      showAlert:false
    },()=>{
      this.props.editReminder(this.state.id.toLocaleString())
    })
    
  }
  renderFAB = ()=>{
    return(
      <Provider>
      <Portal>
        <FAB.Group
          fabStyle={styles.fab}
          open={this.state.open}
          icon={'plus'}
          actions={[
            { icon: 'refresh', color:'#005ecb', label: 'Refresh Calendar', onPress: () => this.props.fetchEvents()},
            { icon: 'bell', color:'#005ecb', label: 'Health Reminder', onPress: () => this.props.addReminder()},
          ]}
          onStateChange={({ open }) => this.setState({ open })}
          onPress={() => {
            if (this.state.open) {
              // do something if the speed dial is open
            }
          }}
        />
      </Portal>
   </Provider>
    );
  }
  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}>
        <Text style={{color:'red'}}>There is no Health Check-Up  Reminder Scheduled for this day</Text>
      </View>
    );
  }

  renderItem = (item)=> {
    const labels =
      item.name &&
      <>
      <View
          key={`label-${item.name}`}
          style={{
            padding: 5,
            backgroundColor:item.color,
            borderRadius: 3,
          }}
        >
        <Text style={{ color: 'white' }}>{item.status}</Text>
        </View>
      </> 

    return (
      <TouchableOpacity 
        onPress={()=>this.handleItemAction(item)}
        style={styles.item}>
        <View>
          <Text
            style={{
              color: item.color,
              fontFamily: fonts.primaryRegular,
              marginBottom: 10,
            }}
          >
            {item.name}
          </Text>
          <Text
            style={{ color: colors.grey, fontFamily: fonts.primaryRegular }}
          >
            {`From:  ${new Date(item.startDate).toUTCString()}`}
          </Text>
          <Text
            style={{ color: colors.grey, fontFamily: fonts.primaryRegular }}
          >
            {`To:       ${new Date(item.endDate).toUTCString()}`}
          </Text>
        </View>
        
       <View styleName="horizontal h-start">{labels}</View>
      </TouchableOpacity>
    );
  }
  
  render() {
    const { events, markedDates } = this.props;
    
    return (
      <View style={styles.container}>
          <Agenda
            items={events}
            renderItem={this.renderItem}
            renderEmptyData={this.renderEmptyDate}
            rowHasChanged={this.rowHasChanged}
            markedDates={markedDates}
            theme={{
              dotColor: colors.primaryLight,
              selectedDayBackgroundColor: colors.primaryLight,
              agendaDayTextColor: colors.primaryLight,
              agendaDayNumColor: colors.primaryLight,
              agendaTodayColor: '#4F44B6',
            }}
          />
        <AwesomeAlert
          show={this.state.showAlert}
          showProgress={false}
          title="SELECT ACTION"
          message="Please Press Outside this Alert to Close it!!"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={true}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="DELETE EVENT"
          confirmText="EDIT EVENT"
          confirmButtonColor="#005ecb"
          cancelButtonColor="#DD6B55"
          onCancelPressed={() => {
            this.handleDelete();
          }}
          onConfirmPressed={() => {
            this.handleEdit();
          }}
        />
        {this.props.isLoading==true ? showDialogProgress('Removing Event from Google Calendar...') : hideDialogProgress()}
        {this.props.eventRemoved &&
          <AwesomeAlert
          show={true}
          showProgress={false}
          titleStyle={{color:colors.green}}
          messageStyle={{color:colors.green}}
          title="SELECT ACTION"
          message={`Event Successfully removed`}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          confirmText='CLOSE'
          confirmButtonColor="#DD6B55"
          onConfirmPressed={()=>{
              this.props.hideAlert()
          }}
        />
        }
        {this.renderFAB()}
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    borderRadius: 5,
    borderWidth:0.1,
    height:100,
    borderColor:colors.white,
    padding: 10,
    marginRight: 10,
    marginTop: 10,
  },
  emptyDate: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
    borderRadius: 5,
    borderWidth:0.1,
    height:100,
    borderColor:colors.white,
    padding: 30,
    marginRight: 10,
    marginTop: 10,
  },
  fab: {
    backgroundColor:'red',
  },
});

export default CalendarScreen;
