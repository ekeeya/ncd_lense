import { connect } from 'react-redux';
import { compose, withState } from 'recompose';

import { fetchAllEvents, editEvent, removeEvent,addReminder, hideDialog } from '../../redux/actions/googleCalendarHandler'

import CalendarScreen from './CalendarView';

function mapStateToProps(state){
  return{
    events: state.CalendarReducer.events,
    markedDates:state.CalendarReducer.markedDates,
    eventRemoved:state.CalendarReducer.eventRemoved,
    eventRemoveError:state.CalendarReducer.eventRemoveError
  }
}

function mapDispatchToProps(dispatch){
  return{
    fetchEvents: () => dispatch(fetchAllEvents()),
    deleteEvent: (id)=>dispatch(removeEvent(id)),
    hideAlert: ()=>dispatch(hideDialog()),
    addReminder:()=>dispatch(addReminder()),
    editReminder:(id)=>dispatch(editEvent(id))
  }
}
export default compose(connect(mapStateToProps, mapDispatchToProps))(CalendarScreen);


/* import { connect } from 'react-redux';
import { compose, withState } from 'recompose';

import { loadItems } from './CalendarState';

import CalendarScreen from './CalendarView';

export default compose(
  withState('tabIndex', 'setTabIndex', 0),
  withState('tabs', 'setTabs', ['Health Calendar', 'Schedule Check-Up']),
  connect(
    state => ({
      items: state.calendar.items
    }),
    dispatch => ({
      loadItems: items => dispatch(loadItems(items)),
    }),
  ),
)(CalendarScreen);
 */
