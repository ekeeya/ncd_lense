import auth  from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
class Fire{
    constructor(){
        //this.init()
        this.checkAuth()
    }


    /* init =()=>{
        if(!firebase.apps.length){
            firebase.initializeApp({
                apiKey: "AIzaSyDQ5cVRXufLutPByI1VUi5qGdhIhQ8eLKs",
                authDomain: "ncd-lens-b0a39.firebaseapp.com",
                databaseURL: "https://ncd-lens-b0a39.firebaseio.com",
                projectId: "ncd-lens-b0a39",
                storageBucket: "ncd-lens-b0a39.appspot.com",
                messagingSenderId: "838620613167",
                appId: "1:838620613167:web:046732e4adfb49ee2214da",
                measurementId: "G-TJZPF2RPRR"
            })
        }
    } 
 */
    checkAuth=()=>{
        auth().onAuthStateChanged(user=>{
            if(!user){
                auth().signInAnonymously();
                console.log("What the fuck")
            }
        })
    }

  
    send = messages =>{
        messages.forEach(item=>{
            const message={
                text: item.text,
                timestamp:database.ServerValue.TIMESTAMP,
                user: item.user
            }
            this.db.push(message)
        })
    }
    parse = message =>{
        const { user, text, timestamp} = message.val()
        const {key:_id} = message
        const createdAt  = new Date(timestamp)
        return {
            _id, 
            createdAt,
            text,
            user
        }
    }

    get = callback =>{
        this.db.on('child_added', snapshot=>callback(this.parse(snapshot)));
    }

    off(){
        this.db.off();
    }

    get db(){
        return database().ref("messages");
    }

    get uid(){
        return (auth().currentUser || {}).uid
    }
}

export default new Fire();