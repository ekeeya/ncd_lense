import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';
import Fire from '../Fire';


class MessagesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages:[]
    };
  }

  get user(){
    return {
      _id: Fire.uid,
      name: "Keeya Emmanuel"
    }
  }

  componentDidMount(){
    Fire.get(message => this.setState(previous =>({
      messages: GiftedChat.append(previous.messages, message)
    })))
  }

  componentWillUnMount(){
      Fire.off();
  }

  render() {
    const chat = <GiftedChat messages = {this.state.messages} onSend={Fire.send} user={this.user}/>

    /* if(Platform.OS === 'android'){
      return (
        <KeyboardAvoidingView style={{flex:1}} behavior="padding" keyboardVerticalOffset={30} enabled>
            {chat}
        </KeyboardAvoidingView>
      );
    } */
    return (
      <SafeAreaView style={{flex:1}}>
          {chat}
      </SafeAreaView>
    );
  }
}

export default MessagesScreen;