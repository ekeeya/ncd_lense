import React, { Component } from 'react';
import { Text, View,Dimensions, StyleSheet, TouchableOpacity } from 'react-native'
//import RateModal from 'react-native-store-rating'
import { Rating, AirbnbRating } from 'react-native-ratings';
import { fonts, colors } from '../../styles';
import { Form, Textarea, Icon} from 'native-base';
import { Button } from '../../components';
import { firebase } from '@react-native-firebase/auth';
import ActivityLoader from '../../components/ActivityLoader'
import ErrorDisplay from '../../components/ErrorDisplay'
import SuccessAlert from '../../components/SuccessAlert';
import AwesomeAlert from 'react-native-awesome-alerts';
import AppLink from 'react-native-app-link';

const { width, height} = Dimensions.get('window')
class FeedBackScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
      rating:3,
      userId:'',
      message:'',
      errorMsg:null
    };
  }

  appLinker = (url, appName, playStoreId, ) => {
    AppLink.maybeOpenURL(url, { appName, playStoreId }).then(() => {
      // do stuff
    })
      .catch((err) => {
        // handle error
      });
  }

  ratingCompleted = (rating) =>{
    this.setState({rating:rating},()=>{
    })
}

checkIfLoggedIn = () => {
  try {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        const {uid} = user._user.providerData[0]
        this.setState({userId:uid})
      }
    })  
  }
  catch (error) {
    console.log(error)
  }
}
resetRating = ()=>{
  this.props.resetRatingState()
  this.setState({
    rating:3,
    message:'',
    errorMsg:null
  })
}
UNSAFE_componentWillMount = ()=>{
  this.checkIfLoggedIn()
}

renderAlert=()=>{
  return(
  <View style={{ alignItems: 'center', justifyContent: 'center', }}>
      <Icon type={"MaterialIcons"} name={"error"} style={{ color:'red',fontSize: 50}}/>
      <View style={{ alignItems:'center',justifyContent:'space-evenly',width:200, alignContent:'space-around'}} >
      <Text style={{alignSelf:'center'}}>{this.state.errorMsg}</Text>
      </View>
  </View>)
} 
_submitFeedback=() =>{
    if(this.state.message ==='' ){
      this.setState({errorMsg:'Please provide a message for us before u can submit this'})
    }else{
      this.props.rateWithFeedback(this.state)
      this.setState({
        rating:3,
        message:'',
        errorMsg:null
      })
    }
}

hideAlert = ()=>{
  this.setState({
    rating:3,
    message:'',
    errorMsg:null
  })
}

  render() {
    return (
			<View style={styles.container}>
        
        {/* <Rating
          showRating
          onFinishRating={this.ratingCompleted}
          style={{ paddingVertical: 10 }}
        /> */}
        <View style={styles.innerStyle}>
          <View style={{marginTop:(height/9)-20}}>
            <Text style={styles.pt}>Share your feedback</Text>
          </View>
        </View>
        <View style={styles.innerLower}>
        <AirbnbRating
              count={5}
              reviews={["Terrible", "Bad", "Good", "Very Good", "Excellent"]}
              defaultRating={this.state.rating}
              size={40}
              onFinishRating={this.ratingCompleted}
            />
        <View style={{justifyContent: 'center',alignSelf:'center'}}>
          <Text style={styles.itemTwoTitle} >Please rate us, then advise us on how to improve</Text>
          </View>
          <View style={{marginTop:20,marginLeft:10,marginRight:10}}>
          <Form>
            <Textarea rowSpan={9} bordered placeholder="Write Feedback Message here" value={this.state.message} onChangeText={(message)=>this.setState({message})} />
          </Form>
          </View>
          <ActivityLoader loading={this.props.working } />
          {this.props.rateError && <ErrorDisplay errorMsg={rateError} reset={this.resetRating}/>}
          {this.props.succesRate && <SuccessAlert reset={this.resetRating} successMsg={'Thank your for this feedback'} />}
          {this.state.errorMsg &&
              <AwesomeAlert
              show={true}
              showProgress={false}
              customView={this.renderAlert()}
              closeOnTouchOutside={false}
              closeOnHardwareBackPress={false}
              showCancelButton={false}
              showConfirmButton={true}
              cancelText="No, cancel"
              confirmText="CLOSE"
              onConfirmPressed={() => {
                this.hideAlert();
              }}
            />
          }
        </View>
        <View style={{alignSelf:'center'}}>
        <Button
            secondary
            rounded
            style={{ width: width * 0.8 }}
            large
            caption="Submit"
            onPress={() => { this._submitFeedback() }}
          />
        </View>
        <View style={{ paddingBottom: 50,margin:10, paddingHorizontal: 10 }}>
            {/* <Title color={colors.lightGray} style={{ marginVertical: 10 }}>
          Share
        </Title> */}
            <View style={{ flexDirection: 'row' }}>
              <Text style={[styles.p]}>Share this Application with tag</Text>
              <Text style={[styles.p,{ marginLeft: 5,color:colors.blue }]}>
                #NcdLens
          </Text>
            </View>
        <View style={{ flexDirection: 'row', marginTop: 15 }}>
            <Button action bgColor="#4867AD" onPress={() => {
              this.appLinker(
                'https://www.facebook.com/sharer/sharer.php?u=https://play.google.com/store/apps/details?id=com.ncdlens&hl=en_GB',
                'Facebook',
                'com.facebook.katana'
            )

             }}>
              <Icon type={'FontAwesome'} name="facebook" style={{size:25,color:'white'}} />
            </Button>
            <Button
              action
              bgColor="#7A3EB1"
              onPress={() => {
                this.appLinker(
                  'https://www.instagram.com',
                  'Instagram',
                  'com.instagram.android'
              )
               }}
              style={{ marginHorizontal: 15 }}
            >
              <Icon type={'FontAwesome'} name="instagram" style={{size:25,color:'white'}} />
            </Button>
            <Button action bgColor={colors.blue} onPress={() => { 
              this.appLinker(
                'https://twitter.com/intent/tweet?url=https://play.google.com/store/apps/details?id=com.ncdlens&hl=en_GB&hashtags=NcdLens',
                'Twitter',
                'com.twitter.android'
            )
            }}>
              <Icon type={'FontAwesome'} name="twitter" style={{size:25,color:'white'}} />
            </Button>
            </View>
          </View>
        
			</View>
		)
  }
}

export default FeedBackScreen;


const styles = StyleSheet.create({
        container:{
            flex:1,
            backgroundColor:'#e3f2fd',
        },
        innerStyle:{
          height:height/2.5,
          backgroundColor:'#f4511e',
          alignContent:'center',
          alignItems:'center'
        },
        innerLower:{
          height:height/2,
          backgroundColor: '#ffff',
          zIndex: 5,
          marginTop: -210,
          margin: 15,
          borderRadius: 10,
          justifyContent: 'center',
        },
        pt: {
          marginVertical: 10,
          letterSpacing: 0,
          color: colors.white,
          fontWeight:'bold',
          fontSize:25
        },
        p: {
          marginVertical: 10,
          letterSpacing: 0,
        },
        itemTwoTitle: {
          color: '#f1c40f',
          fontFamily: fonts.primaryBold,
          fontSize: 15,
        },
});
