// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { addFeedback, resetRating } from '../../redux/actions/addFeedback' 
import FeedBackScreen from './FeedBackView';


function mapStateToProps(state){
    return{
        working:state.addFeedback.working,
        rateError:state.addFeedback.rateError,
        succesRate:state.addFeedback.succesRate,
    }
}

function mapDispatchToProps(dispatch){
    return{
        rateWithFeedback:(state, fromFirebae) =>dispatch(addFeedback(state,fromFirebae)),
        resetRatingState: ()=>dispatch(resetRating())
    }
}
export default compose(connect(mapStateToProps,mapDispatchToProps))(FeedBackScreen);
