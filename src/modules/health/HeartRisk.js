import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  StatusBar,
  Dimensions,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import AppLink from 'react-native-app-link';
import Icon from 'react-native-vector-icons/FontAwesome';
import ErrorDisplay from '../../components/ErrorDisplay'
import { TextInput, RadioButton, FAB, Portal, Provider } from 'react-native-paper';
import AwesomeAlert from 'react-native-awesome-alerts';
import ActivityLoader from '../../components/ActivityLoader'
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';

import { colors } from '../../styles';
import { Dropdown, Button } from '../../components';
import { Text, Title, Caption } from '../../components/StyledText';

const { width, height } = Dimensions.get('window')
class HeartRiskScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      recommendation: [],
      showMore: false,
      displayName: '',
      active: false,
      bloodPressure: '',
      height: '',
      weight: '',
      bloodGlucose: '',
      bloodGlucoseUnit: 'mmol/l',
      userId: this.props.navigation.state.params, //from database
      uid: null,  //from firebase
      healthData: null,
      isVisible: false,
      showHealthForm: false
    }
  }
  /* const _renderRecommendationCard = ({ item }) => (
    <TouchableOpacity style={styles.recommendationItem}>
      <View style={styles.recommendationItemTopContainer}>
        {item.badge && (
          <View style={styles.recommendationItemBadge}>
            <Caption white size={12}>
              {item.badge}
            </Caption>
          </View>
        )}

        <View style={styles.recommendationItemRating}>
          <Caption bold color={colors.yellow}>
            {item.rating}
          </Caption>
        </View>
      </View>
      <Image
        style={{ width: 150, height: 100 }}
        source={require('../../../assets/images/nike1.png')}
        resizeMode="contain"
      />
      <View style={styles.recommendationBody}>
        <Text color={colors.gray} style={styles.recommendationTitle}>
          {item.title}
        </Text>
        <Text color={colors.primaryLight}>{item.brand}</Text>
        <View style={{ marginTop: 5, flexDirection: 'row' }}>
          {item.oldPrice && (
            <Text lineThrough color={colors.gray} style={{ marginRight: 10 }}>
              {item.oldPrice}
            </Text>
          )}
          <Text color={item.oldPrice ? colors.secondary : colors.gray}>
            {item.price}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  ); */

  appLinker = (url, appName, playStoreId, ) => {
    AppLink.maybeOpenURL(url, { appName, playStoreId }).then(() => {
      // do stuff
    })
      .catch((err) => {
        // handle error
      });
  }

  resetHeath = () => {
    this.props.resetHealthAdd()
  }
  hideAlert = () => {
    this.setState({
      showHealthForm: false
    });
  };
  resetSubmit = () => {
    this.props.resetHealthAdd()
  }
  _showHealthForm = () => {
    this.setState({ showHealthForm: true })
  }

  onSubmit = () => {
    null;

  }


  renderFAB = (itemParams) => {
    return (
      <Provider>
        <Portal>
          <FAB.Group
            fabStyle={styles.fab}
            open={this.state.open}
            icon={'pencil'}
            actions={[
              { icon: 'pencil', color: '#005ecb', label: `Edit ${(itemParams.title).toLowerCase()}`, onPress: () => this._showHealthForm() },
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
              }
            }}
          />
        </Portal>
      </Provider>
    );
  }
  renderHealthForm = (itemParams) => {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center', height: height / 5 }}>
        <View style={{ alignItems: 'center' }}>
          <View style={styles.formView}>
            <TextInput
              label={`${itemParams.title}`}
              underlineColor={'#F44336'}
              selectionColor={'#F44336'}
              value={this.state.bloodPressure}
              style={styles.inputView}
              onChangeText={(bloodPressure) => this.setState({ bloodPressure })}
            />
          </View>

        </View>

      </View>
    )
  }

  UNSAFE_componentWillMount = () => {
    const { title, status, client_id } = this.props.navigation.state.params
    this.props.fetchRec(title, status)
    console.log(this.props)
  }

  toggleShowMore = () => {
    this.setState({ showMore: !this.state.showMore })
  }
  render() {
    const itemParams = this.props.navigation.state.params;
    let info;
    let less;
    let more;
    if (this.props.recommendation.length > 360) {
      info = this.props.recommendation.split("\n")
      less = info[0]
      for (let i = 0; i < info.length; i++) {
        if (i > 0) {
          if (info[i] != "") {
            more += `\n${info[i]}`

          }
        }
      }
    } else {
      less = this.props.recommendation
    }

    return (
      <View style={styles.container}>
        <ScrollView >
          <StatusBar backgroundColor="red" barStyle="light-content" />
          <View style={styles.header}>
            <View
              style={{
                flexDirection: 'column',
                flex: 1,
              }}
            >
              <Text style={{ color: colors.primaryLight }}>
                {itemParams.title}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignSelf: 'stretch',
                  marginTop: 8,
                }}
              >
                <Title bold color={itemParams.color}>
                  {itemParams.status}
                </Title>
                <Title bold color={itemParams.color}>
                  {itemParams.measure}
                </Title>
              </View>
            </View>
          </View>
          <View style={styles.carouselContainer}>
            <Carousel
              autoplay
              sliderWidth={Dimensions.get('window').width - 30}
              itemWidth={Dimensions.get('window').width}
              renderItem={({ item }) => (
                <Image
                  resizeMode="contain"
                  style={{ height: 250, width: Dimensions.get('window').width }}
                  source={item}
                />
              )}
              data={[
                require('../../../assets/images/slimming.png'),
                require('../../../assets/images/diabets.jpg'),
              ]}
            />
            {itemParams.badge && (
              <View style={styles.badge}>
                <Caption white bold>
                  {itemParams.badge}
                </Caption>
              </View>
            )}
          </View>
          <View style={styles.bodyContainer}>
            <View style={styles.bodyHeading}>
              <Title color={colors.gray} size={23}>
                {itemParams.price}
              </Title>
              {/* <Caption underline size={15} color={colors.lightGray}>
            Free delivery & returns
          </Caption> */}
            </View>
            <View style={{ paddingTop: 20 }}>

            </View>
            <View style={styles.buttonsSection}>
              <View style={{ flex: 3 }}>
                <Button onPress={() => alert("This feature is under development")} secondary caption="Chat With Physician" rounded />
              </View>

            </View>
            <View style={styles.description}>
              <Title bold color={colors.lightGray} size={17}>
                EXPERT RECOMENDATIONS
          </Title>
              <Text style={styles.p}>
                {less}
              </Text>
              {this.state.showMore &&
                <Text style={styles.p}>
                  {more}
                </Text>
              }
              <View style={{ display: 'none' }}>

              </View>
            </View>
            <View style={{ alignItems: 'center', paddingVertical: 15 }}>
              <Button
                onPress={() => { this.toggleShowMore() }}
                bordered
                bgColor={colors.grey}
                textColor={colors.gray}
                caption={this.state.showMore ? "READ LESS" : "READ MORE"}
                style={{
                  width: 200,
                }}
              />
            </View>
          </View>
          {/* <View style={styles.recommendationsContainer}>
        <Title color={colors.lightGray} style={{ marginVertical: 10 }}>
          YOU MIGHT ALSO LIKE
        </Title>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal
          data={recommendations}
          keyExtractor={item => `${item.id}`}
          renderItem={_renderRecommendationCard}
        />
      </View> */}

          <View style={{ paddingBottom: 50, paddingHorizontal: 15 }}>
            {/* <Title color={colors.lightGray} style={{ marginVertical: 10 }}>
          Share
        </Title> */}
            <View style={{ flexDirection: 'row' }}>
              <Text color={colors.gray}>Share this Application with tag</Text>
              <Text color={colors.blue} style={{ marginLeft: 5 }}>
                #NcdLens
          </Text>
            </View>

            <View style={{ flexDirection: 'row', marginTop: 15 }}>
              <Button action bgColor="#4867AD" onPress={() => {
                this.appLinker(
                  'https://www.facebook.com',
                  'Facebook',
                  'com.facebook.katana'
                )

              }}>
                <Icon name="facebook" size={25} color="white" />
              </Button>
              <Button
                action
                bgColor="#7A3EB1"
                onPress={() => { }}
                style={{ marginHorizontal: 15 }}
              >
                <Icon name="instagram" size={25} color="white" />
              </Button>
              <Button action bgColor={colors.blue} onPress={() => {
                this.appLinker(
                  'https://twitter.com/elkeeya',
                  'Twitter',
                  'com.twitter.android'
                )
              }}>
                <Icon name="twitter" size={25} color="white" />
              </Button>

            </View>
          </View>
          <AwesomeAlert
            show={this.state.showHealthForm}
            title={`EDIT ${itemParams.title}`}
            showProgress={false}
            customView={this.renderHealthForm(itemParams)}
            closeOnTouchOutside={true}
            closeOnHardwareBackPress={false}
            showCancelButton={true}
            showConfirmButton={true}
            confirmButtonColor="green"
            cancelButtonColor="#DD6B55"
            cancelText="No, cancel"
            confirmText="Continue"
            onConfirmPressed={() => {
              this.hideAlert();
            }}
            onCancelPressed={() => {
              this.hideAlert();
            }}
          />
          {this.props.recError &&
            <ErrorDisplay errorMsg={this.props.recError} reset={this.resetHeath} />
          }
          <ActivityLoader loading={this.props.isFetching} />

        </ScrollView>
        {this.renderFAB(itemParams)}
      </View>
    );
  }
}

export default HeartRiskScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  carouselContainer: {
    marginVertical: 10,
    paddingHorizontal: 15,
  },
  bodyContainer: {
    paddingHorizontal: 15,
  },
  bodyHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  sizeDropdownContainer: {
    flex: 2,
    paddingVertical: 10,
    paddingRight: 5,
  },
  quantityDropdownContainer: {
    flex: 1,
    paddingVertical: 10,
    paddingLeft: 5,
  },
  buttonsSection: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  actionButtonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  p: {
    marginVertical: 5,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
  },
  description: {
    paddingTop: 10,
    marginVertical: 10,
  },
  recommendationsContainer: {
    backgroundColor: colors.white,
    marginTop: 10,
    paddingHorizontal: 15,
  },
  recommendationItem: {
    marginVertical: 10,
    paddingBottom: 10,
    marginRight: 15,
    borderWidth: 0.7,
    borderColor: colors.lightGray,
  },
  recommendationBody: {
    backgroundColor: 'white',
    padding: 8,
  },
  recommendationTitle: {
    marginVertical: 5,
  },
  badge: {
    paddingHorizontal: 10,
    paddingVertical: 3,
    backgroundColor: colors.green,
    position: 'absolute',
    left: 15,
    top: 0,
  },
  recommendationItemTopContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingRight: 5,
  },
  recommendationItemBadge: {
    backgroundColor: colors.secondary,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  recommendationItemRating: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 3,
  },
  fab: {
    backgroundColor: '#004ecb',
  },
  formView: {
    paddingHorizontal: 20,
    marginTop: 5,
    width: width * 0.8,
    justifyContent: 'space-between'
  },
});
