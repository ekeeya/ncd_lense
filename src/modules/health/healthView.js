import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
  StatusBar,
  Dimensions,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import AppLink from 'react-native-app-link';
import { Icon } from 'native-base'
import ErrorDisplay from '../../components/ErrorDisplay'
import { TextInput, RadioButton, FAB, Portal, Provider } from 'react-native-paper';
import AwesomeAlert from 'react-native-awesome-alerts';
import ActivityLoader from '../../components/ActivityLoader'

import { colors } from '../../styles';
import { Dropdown, Button } from '../../components';
import { Text, Title, Caption } from '../../components/StyledText';

const { width, height } = Dimensions.get('window')
export default class HealthScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      recommendation: [],
      showMore: false,
      displayName: '',
      active: false,
      editValue: '',
      height: '',
      weight: '',
      bloodGlucose: '',
      bloodGlucoseUnit: 'mmol/l',
      userId: this.props.navigation.state.params, //from database
      uid: null,  //from firebase
      healthData: null,
      isVisible: false,
      showHealthForm: false,
      showWHORecommendation: true,
      whoMore: false
    }
  }
  

  appLinker = (url, appName, playStoreId, ) => {
    AppLink.maybeOpenURL(url, { appName, playStoreId }).then(() => {
      // do stuff
    })
      .catch((err) => {
        // handle error
      });
  }

  resetHeath = () => {
    this.props.resetHealthAdd()
  }
  hideAlert = () => {
    this.setState({
      showHealthForm: false,
      showWHORecommendation: false,
    });
  };
  resetSubmit = () => {
    this.props.resetHealthAdd()
  }
  _showHealthForm = () => {
    this.setState({ showHealthForm: true })
  }

  onSubmit = () => {
    null;

  }

  calculateAge = (dateString) => {
    let today = new Date();
    let birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  renderFAB = (itemParams) => {
    return (
      <Provider>
        <Portal>
          <FAB.Group
            fabStyle={styles.fab}
            open={this.state.open}
            icon={'pencil'}
            actions={[
              { icon: 'pencil', color: '#005ecb', label: `Edit ${(itemParams.title).toLowerCase()}`, onPress: () => this._showHealthForm() },
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
              }
            }}
          />
        </Portal>
      </Provider>
    );
  }
  renderHealthForm = (itemParams) => {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center', height: height / 5 }}>
        <View style={{ alignItems: 'center' }}>
          <View style={styles.formView}>
            <TextInput
              label={`${itemParams.title}`}
              underlineColor={'#F44336'}
              selectionColor={'#F44336'}
              value={(itemParams.measure).split(" ")[0]}
              style={styles.inputView}
              onChangeText={(editValue) => this.setState({ editValue })}
            />
          </View>

        </View>

      </View>
    )
  }
  renderWhoRecommendations = () => {
    const itemParams = this.props.navigation.state.params;

    let systolic = (this.props.healthRec[0].measure).split('/')[0]
    let glucoseStatus = this.props.healthRec[2].status
    let glucoseMeasure = this.props.healthRec[2].measure
    let DOB =  this.props.bioInfo.dob
    let isDiabetic = 'No';
    if (glucoseStatus === 'BAD:Hyperglycemic') {
      isDiabetic = 'Yes'
    }
    let record;
    let recLess;
    let recMore;
    let colorCode = '#fff';
    let risk = 'Unknown'
    if (this.props.whoRecommendation !== null){
      colorCode = this.props.whoRecommendation.color_code
      risk = this.props.whoRecommendation.percent_risk
      if (this.props.whoRecommendation.recommendation.length > 100) {
        record = this.props.whoRecommendation.recommendation.split("\n")
        recLess = record[0]
        for (let i = 0; i < record.length; i++) {
          if (i > 0) {
            if (record[i] != "") {
              recMore += `\n${record[i]}`
            }
          } 
        }
      } else {
        recLess = this.props.whoRecommendation.recommendation
      }
    }

    return (
      <View style={{ alignItems: 'center', height: this.state.whoMore ? height / 1.5:height / 2 , marginLeft: 10, marginRight: 10 }}>
        <View style={[styles.whoStatusView, { backgroundColor:colorCode  }]}>
          <Title color={colorCode.toLowerCase() === 'yellow' ? 'black' :'white'} size={21}>
            {risk}
          </Title>
          <Text style={[styles.p, { color: colorCode.toLowerCase() === 'yellow' ? 'black' :'white'  }]}>of a cardiovascular(Heart) problem</Text>
        </View>
        <Caption bold color={colors.yellow} size={15}>
          Below are your details
        </Caption>
        <ScrollView horizontal={true} style={[styles.whoStats]}>
          <View style={styles.itemDetail}>
            <Caption bold color={colors.blue}>
              Gender
          </Caption>
            <Text color={colors.primary}>{this.props.bioInfo.gender}</Text>
          </View >
          <View style={styles.itemDetail}>
            <Caption bold color={colors.blue}>
              Age
          </Caption>
          <Text color={colors.primaryLight}>{this.calculateAge(DOB)}</Text>
          </View>
          <View style={styles.itemDetail}>
            <Caption bold color={colors.blue}>
              Is Smoker?
          </Caption>
            {this.props.bioInfo.smoke === true ?
              <Text style={{ alignSelf: 'center' }} color={colors.primaryLight}>Yes</Text>
              :
              <Text style={{ alignSelf: 'center' }} color={colors.green}>No</Text>
            }
          </View>
          <View style={styles.itemDetail}>
            <Caption bold color={colors.blue}>
              Systolic BP
          </Caption>
            <Text style={{ alignSelf: 'center' }} color={colors.primaryLight}>{systolic}</Text>
          </View>
          <View style={styles.itemDetail}>
            <Caption bold color={colors.blue}>
              Diabetic
          </Caption>
            {isDiabetic === 'Yes' ?
              <>
                <Text style={{ alignSelf: 'center' }} color={colors.primaryLight}>{`${isDiabetic}`}</Text>
                {/* <Text style={{ alignSelf: 'center' }} color={colors.primaryLight}>{`(${glucoseMeasure})`}</Text> */}
              </>
              :
              <>
                <Text style={{ alignSelf: 'center' }} color={colors.green}>{`${isDiabetic}`}</Text>
                {/* <Text style={{ alignSelf: 'center' }} color={colors.green}>{`(${glucoseMeasure})`}</Text> */}
              </>
            }

          </View>
        </ScrollView>
        <Caption bold color={colors.yellow} size={15} style={{ marginTop: 5 }}>
          Description
        </Caption>
        <Title style={styles.p} size={13} color={`#000000`}>
          This is a 10-year risk assessment of a fatal or non-fatal cardiovascular event basing on your gender, age, systolic blood pressure,
          smoking status and presence or absence of diabetes mellitus.
        </Title>
        <Caption bold color={colors.yellow} size={15} style={{ marginTop: 5,alignSelf:'center' }}>
          Recommendation
        </Caption>
        <ScrollView style={{ marginTop: 10 }}>
            <Text style={styles.p}>
              {this.props.fetchRecError ? this.props.fetchRecError : recLess }
            </Text>
            {this.state.whoMore &&
            <Text style={styles.p}>{recMore}</Text>
            }
        </ScrollView>
        <View style={styles.whoTextView}>
          <Button action bgColor={colors.blue}
            onPress={() => this.setState({ whoMore: !this.state.whoMore })}
          >
            {this.state.whoMore ? <Icon type="AntDesign" name="upcircle" style={{ size: 20, color: "white" }} /> : <Icon type="AntDesign" name="downcircle" style={{ size: 20, color: "white" }} />}
          </Button>
          {this.state.whoMore ? <Text color={colors.blue}>Read Less</Text> : <Text color={colors.blue}>Read More</Text>}
        </View>

      </View>
    )
  }
  UNSAFE_componentWillMount = () => {
    const { title, status, client_id } = this.props.navigation.state.params
    this.props.fetchRec(title, status)
    //const itemParams = this.props.navigation.state.params;
    let systolic = parseInt((this.props.healthRec[0].measure).split('/')[0])
    const glucoseStatus = this.props.healthRec[2].status
    const gender = this.props.bioInfo.gender
    const isSmoker = this.props.bioInfo.smoke
    let age =  parseInt(this.calculateAge(this.props.bioInfo.dob))
    let isDiabetic = false;
    if (glucoseStatus === 'BAD:Hyperglycemic') {
      isDiabetic = true
    }
    if(age > 39){
      this.props.fetchWHOAssess(gender,age,systolic,isSmoker,isDiabetic)
    }
  }

  toggleShowMore = () => {
    this.setState({ showMore: !this.state.showMore })
  }
  render() {
    const age =  parseInt(this.calculateAge(this.props.bioInfo.dob))
    const itemParams = this.props.navigation.state.params;
    let info;
    let less;
    let more;
    if (this.props.recommendation.length > 360) {
      info = this.props.recommendation.split("\n")
      less = info[0]
      for (let i = 0; i < info.length; i++) {
        if (i > 0) {
          if (info[i] != "") {
            more += `\n${info[i]}`

          }
        }
      }
    } else {
      less = this.props.recommendation
    }

    return (
      <View style={styles.container}>
        <ScrollView >
          <StatusBar backgroundColor="red" barStyle="light-content" />
          <View style={styles.header}>
            <View
              style={{
                flexDirection: 'column',
                flex: 1,
              }}
            >
              <Text style={{ color: colors.primaryLight }}>
                {itemParams.title}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignSelf: 'stretch',
                  marginTop: 8,
                }}
              >
                <Title bold color={itemParams.color}>
                  {itemParams.status}
                </Title>
                <Title bold color={itemParams.color}>
                  {itemParams.measure}
                </Title>
              </View>
            </View>
          </View>
          <View style={styles.carouselContainer}>
            <Carousel
              autoplay
              sliderWidth={Dimensions.get('window').width}
              itemWidth={Dimensions.get('window').width}
              renderItem={({ item }) => (
                <Image
                  resizeMode="contain"
                  style={{ height: 250, width: Dimensions.get('window').width }}
                  source={item}
                />
              )}
              data={[
                require('../../../assets/images/slimming.png'),
                require('../../../assets/images/diabets.jpg'),
              ]}
            />
            {itemParams.badge && (
              <View style={styles.badge}>
                <Caption white bold>
                  {itemParams.badge}
                </Caption>
              </View>
            )}
          </View>
          <View style={styles.bodyContainer}>
            <View style={styles.bodyHeading}>
              <Title color={colors.gray} size={23}>
                {itemParams.price}
              </Title>
              {/* <Caption underline size={15} color={colors.lightGray}>
            Free delivery & returns
          </Caption> */}
            </View>
            <View style={styles.buttonsSection}>
              <View style={{ flex: 3 }}>
                <Button onPress={() => this.appLinker(
            'https://api.whatsapp.com/send?phone=+256706794776',
            'WhatsApp',
            'com.whatsapp'
          )} secondary caption="Chat With Physician" rounded />
              </View>
            </View>
            <View style={styles.description}>
              <Title bold color={colors.lightGray} size={17}>
                EXPERT RECOMENDATIONS
          </Title>
              <Text style={styles.p}>
                {less}
              </Text>
              {this.state.showMore &&
                <Text style={styles.p}>
                  {more}
                </Text>
              }
              <View style={{ display: 'none' }}>

              </View>
            </View>
            <View style={{ alignItems: 'center', paddingVertical: 15 }}>
              <Button
                onPress={() => { this.toggleShowMore() }}
                bordered
                bgColor={colors.grey}
                textColor={colors.gray}
                caption={this.state.showMore ? "READ LESS" : "READ MORE"}
                style={{
                  width: 200,
                }}
              />
            </View>
          </View>
          {
            age > 39 &&
            <View style={styles.recommendationsContainer}>
            <Title bold color={colors.lightGray} size={17}>
              WORLD HEALTH ORGANIZATION RISK PREDICTION
            </Title>
            <Text style={styles.p}>
              You are <Text style={{ fontWeight: 'bold' }} >{age} years</Text> old, we advise you look at WHO's cardiovascular event prediction below
            </Text>
                <Text style={styles.p}>
                  This is a 10-year risk of a fatal or non-fatal cardiovascular event by gender, age, systolic blood pressure,
                  smoking status and presence of absence of diabetes mellitus.
            </Text>
            <View style={{ alignItems: 'center', paddingVertical: 15 }}>
              <Button
                onPress={() => this.setState({ showWHORecommendation: true })}
                bordered
                bgColor={colors.yellow}
                textColor={colors.yellow}
                caption={"VIEW MORE"}
                style={{
                  width: 200,
                }}
              />
            </View>
          </View>
          }
          
          <View style={{ paddingBottom: 50, paddingHorizontal: 15 }}>
            {/* <Title color={colors.lightGray} style={{ marginVertical: 10 }}>
          Share
        </Title> */}
            <View style={{ flexDirection: 'row' }}>
              <Text color={colors.gray}>Share this Application with tag</Text>
              <Text color={colors.blue} style={{ marginLeft: 5 }}>
                #NcdLens
          </Text>
            </View>

            <View style={{ flexDirection: 'row', marginTop: 15 }}>
              <Button action bgColor="#4867AD" onPress={() => {
                this.appLinker(
                  'https://www.facebook.com',
                  'Facebook',
                  'com.facebook.katana'
                )

              }}>
                <Icon type="FontAwesome" name="facebook" style={{ size: 25, color: "white" }} />
              </Button>
              {/* <Button
                action
                bgColor="#7A3EB1"
                onPress={() => { }}
                style={{ marginHorizontal: 15 }}
              >
                <Icon type="FontAwesome" name="instagram" style={{ size: 25, color: "white" }} />
              </Button> */}
              <Button action bgColor={colors.blue} 
              style={{paddingHorizontal:10}}
              onPress={() => {
                this.appLinker(
                  'https://twitter.com/elkeeya',
                  'Twitter',
                  'com.twitter.android'
                )
              }}>
                <Icon type="FontAwesome" name="twitter" style={{ size: 25, color: "white" }} />
              </Button>

            </View>
          </View>
          {this.props.recError &&
            <ErrorDisplay errorMsg={this.props.recError} reset={this.resetHeath} />
          }
          <ActivityLoader loading={this.props.isFetching} />

        </ScrollView>
        {/* this.renderFAB(itemParams) */}
        <AwesomeAlert
          show={this.state.showHealthForm}
          title={`EDIT ${itemParams.title}`}
          showProgress={false}
          customView={this.renderHealthForm(itemParams)}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={true}
          confirmButtonColor="green"
          cancelButtonColor="#DD6B55"
          cancelText="No, cancel"
          confirmText="Continue"
          onConfirmPressed={() => {
            this.hideAlert();
          }}
          onCancelPressed={() => {
            this.hideAlert();
          }}
        />
        {
          age > 39 &&
          <AwesomeAlert
          show={this.state.showWHORecommendation}
          title={`WORLD HEALTH ORGANIZATION RISK PREDICTION`}
          titleStyle={styles.alertTitle}
          showProgress={false}
          customView={this.renderWhoRecommendations()}
          closeOnTouchOutside={false}
          alertContainerStyle={{ width: width }}
          closeOnHardwareBackPress={false}
          showCancelButton={true}
          showConfirmButton={false}
          confirmButtonColor="green"
          cancelButtonColor={colors.secondaryGradientStart}
          cancelText="Close"
          onCancelPressed={() => {
            this.hideAlert();
          }}
        />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: 'white',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  carouselContainer: {
    marginVertical: 10,
    paddingHorizontal: 15,
  },
  whoStatusView: {
    width: width * 0.75,
    marginBottom: 5,
    justifyContent: 'center',
    borderRadius: 5,
    height: 50,
    alignItems: 'center'
  },
  whoStats: {
    width: width * 0.75,
    borderRadius: 2,
    height:110,
    alignContent:'flex-start',
    flexDirection: 'row',
    borderWidth: 0.1,
    padding: 12
  },
  whoTextView: {
    width: width * 0.75,
    alignItems: 'center'
  },
  bodyContainer: {
    paddingHorizontal: 15,
  },
  bodyHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  sizeDropdownContainer: {
    flex: 2,
    paddingVertical: 10,
    paddingRight: 5,
  },
  quantityDropdownContainer: {
    flex: 1,
    paddingVertical: 10,
    paddingLeft: 5,
  },
  buttonsSection: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  actionButtonContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  p: {
    marginVertical: 5,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
  },
  alertTitle: {
    marginVertical: 5,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
    fontSize: 10,
    fontWeight: 'bold'
  },
  description: {
    paddingTop: 10,
    marginVertical: 10,
  },
  recommendationsContainer: {
    backgroundColor: colors.white,
    marginTop: 10,
    paddingHorizontal: 15,
  },
  recommendationItem: {
    marginVertical: 10,
    paddingBottom: 10,
    marginRight: 15,
    borderWidth: 0.7,
    borderColor: colors.lightGray,
  },
  recommendationBody: {
    backgroundColor: 'white',
    padding: 8,
  },
  recommendationTitle: {
    marginVertical: 5,
  },
  badge: {
    paddingHorizontal: 10,
    paddingVertical: 3,
    backgroundColor: colors.green,
    position: 'absolute',
    left: 15,
    top: 0,
  },
  recommendationItemTopContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingRight: 5,
  },
  recommendationItemBadge: {
    backgroundColor: colors.secondary,
    paddingVertical: 3,
    paddingHorizontal: 10,
  },
  recommendationItemRating: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingVertical: 3,
  },
  fab: {
    backgroundColor: '#004ecb',
  },
  formView: {
    paddingHorizontal: 20,
    marginTop: 5,
    width: width * 0.8,
    justifyContent: 'space-between'
  },
  itemDetail: {
    paddingHorizontal: 8,
  }
});
