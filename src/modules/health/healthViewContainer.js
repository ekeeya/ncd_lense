// @flow
import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import {getRecommendation} from '../../redux/actions/getRecommendation'
import { addNewHealthRecord, resetHealthAddState } from '../../redux/actions/addHealthRecord'
import { getWHORecommendation } from '../../redux/actions/WHORecommendation';
import HealthScreen from './healthView';

function mapStateToProps(state){
  return{
      isFetching:state.getRecommendation.isFetching,
      recError:state.getRecommendation.recError,
      recommendation:state.getRecommendation.recommendation,
      user:state.signInWithFirebase.user,
      bioInfo:state.checkQuestionier.bioInfo,
      healthRec:state.UserhealthRecord.healthRec,
      isFetchingRec: state.getWHORecommendation.isFetchingRec,
      whoRecommendation:state.getWHORecommendation.whoRecommendation,
      fetchRecError:state.getWHORecommendation.fetchRecError
      }
}
function mapDispatchToProps(dispatch){
   return{
       fetchRec: (category,status)=> dispatch(getRecommendation(category,status)),
       addNewrecord:(state)=>dispatch(addNewHealthRecord(state)),
       resetHealthAdd:()=>dispatch(resetHealthAddState()),
       fetchWHOAssess:(gender,age,systolic,isSmoker,isDiabetic)=>dispatch(getWHORecommendation(gender,age,systolic,isSmoker,isDiabetic))
}
}

export default compose(
  connect(mapStateToProps,mapDispatchToProps ),
  withState('selectedSizeIndex', 'setSelectedSizeIndex', -1),
  withState('selectedQuantityIndex', 'setSelectedQuantityIndex', -1),
)(HealthScreen);
