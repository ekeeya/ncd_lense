import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  StatusBar,
  Alert,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons';
//import { DatePicker, Content } from 'native-base'
import DatePicker from 'react-native-datepicker'
import LinearGradient from 'react-native-linear-gradient';
import { TextInput, RadioButton, Checkbox, FAB, Portal, Provider } from 'react-native-paper';
import { Button, GridRow } from '../../components';
import { fonts, colors } from '../../styles';
import RNBottomActionSheet from 'react-native-bottom-action-sheet';
import AwesomeAlert from 'react-native-awesome-alerts';
import AppLink from 'react-native-app-link';
import ActivityLoader from '../../components/ActivityLoader'
import ErrorDisplay from '../../components/ErrorDisplay'
import SuccessAlert from '../../components/SuccessAlert';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import { showDialogProgress, hideDialogProgress } from '../../components/utils';
import PureChart from 'react-native-pure-chart';



const listData = [
  {
    id: 1,
    title: 'BLOOD PRESSURE',
    status: 'CLICK TO ADD',
    measure: '',
    image: require('../../../assets/images/bp_doc.jpg'),
    color: '#1faa00'
  },
  {
    id: 2,
    title: 'BODY MASS INDEX',
    status: 'CLICK TO ADD',
    measure: '',
    image: require('../../../assets/images/health_fat.jpg'),
    color: 'red'
  }];

const bloodGlucoseBar = [
  {
    id: 3,
    title: 'BODY GLUCOSE',
    status: 'CLICK TO ADD',
    measure: '',
    image: require('../../../assets/images/glucose.jpg'),
    colors: ['#00acee', '#1877f2']
  }
]
const { width, height } = Dimensions.get('window')
class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      displayName: '',
      active: false,
      bloodPressure: '',
      height: '',
      weight: '',
      bloodGlucose: '',
      bloodGlucoseUnit: 'mmol/l',
      userId: null, //from database
      uid: null,  //from firebase
      healthData: null,
      isVisible: false,
      showHealthForm: false,
      showQuestions: this.props.isNewUser,
      contigious: false,
      ncdKnowLevel: [],
      checked: false,
      check1: false,
      check2: false,
      check3: false,
      check4: false,
      measuredBp: false,
      measuredSugar: false,
      measuredBMI: false,
      smokes: false,
      cigarateNo: 0,
      drinkAlcohol: false,
      dailyBottles: 0,
      fromFirebase: 1,
      dob: '',
      isMale: false,
      isFemale: false,
      gender: '',
      heathRecords: []

    }
  }
  setDate = (newDate) => {
    console.log(newDate)
    this.setState({ dob: newDate });
  }

  hideAlert = () => {
    this.setState({
      showHealthForm: false
    });
  };
  resetHeath = () => {
    this.props.resetHealth()
  }
  resetSubmit = () => {
    this.props.resetHealthAdd()
    this.props.fetchlatestRec(this.state.uid, 1)
    this.props.fecthHeathRecs(this.state.uid, 1)
  }

  resetInitialInfoData = () => {
    this.setState({ showQuestions: false })
    this.props.resetQuestionaireAdd()
    this.props.fetchlatestRec(this.state.uid, 1)
    this.props.fecthHeathRecs(this.state.uid, 1)
  }

  popFromArray = item => {
    let array = this.state.ncdKnowLevel
    const index = array.indexOf(item);
    if (index > -1) {
      array.splice(index, 1);
    }
    this.setState({ ncdKnowLevel: array })
  }
  toggleGender = () => {
    if (this.state.isMale) {
      this.setState({ isFemale: false, gender: 'Male' })
    }
    else if (this.state.isMale === false) {
      this.setState({ isFemale: true, gender: 'Female' })
    }

  }
  toggleChecks = _check => {
    let array = []
    if (_check === 'check1') {
      this.setState({ check1: !this.state.check1 }, () => {
        if (this.state.check1) {
          let joined = this.state.ncdKnowLevel.concat('I just hear about them')
          this.setState({ ncdKnowLevel: joined })
        }
        else if (this.state.check1 === false) {
          this.popFromArray('I just hear about them')
        }
      })
    } else if (_check === 'check2') {
      this.setState({ check2: !this.state.check2 }, () => {
        if (this.state.check2) {
          let joined = this.state.ncdKnowLevel.concat('I have ever read about them')
          this.setState({ ncdKnowLevel: joined })
        }
        else if (this.state.check2 === false) {
          this.popFromArray('I have ever read about them')
        }
      })
    } else if (_check === 'check3') {
      this.setState({ check3: !this.state.check3 }, () => {
        if (this.state.check3) {
          let joined = this.state.ncdKnowLevel.concat('I have been diagnosed with one of them')
          this.setState({ ncdKnowLevel: joined })
        }
        else if (this.state.check3 === false) {
          this.popFromArray('I have been diagnosed with one of them')
        }
      })
    } else if (_check === 'check4') {
      this.setState({ check4: !this.state.check4 }, () => {
        if (this.state.check4) {
          let joined = this.state.ncdKnowLevel.concat('I don\'t know anything about them')
          this.setState({ ncdKnowLevel: joined })
        }
        else if (this.state.check4 === false) {
          this.popFromArray('I don\'t know anything about them')
        }
      })
    }
  }

  appLinker = (url, appName, playStoreId, ) => {
    AppLink.maybeOpenURL(url, { appName, playStoreId }).then(() => {
      // do stuff
    })
      .catch((err) => {
        // handle error
      });
  }

  submitQuestionaire = () => {
    if (this.state.dob === null) {
      Alert.alert(
        "Provide Date of Birth",
        "Date of birth can't be empty")
    }
    else if (this.state.gender === '') {
      Alert.alert(
        "Please Select Gender",
        "Gender can't be empty")
    } else {
      this.props.resetQuestionierCheck()
      this.props.addInitialInfo(this.state, 1)
      setTimeout(() => {
        this.props.fetchlatestRec(this.state.uid, 1)
      }, 50)
    }
  }

  UNSAFE_componentWillMount = () => {
    const userNames = this.props.navigation.state.params.displayName ? this.props.navigation.state.params.displayName : this.props.navigation.state.params.name
    const uid = this.props.navigation.state.params.uid ? this.props.navigation.state.params.uid : this.props.navigation.state.params.id
    if (uid) {
      this.setState({ uid: uid })
      // i know i used firebase
      if (this.props.hasQuestionierData) {
        this.setState({ showQuestions: false })
        this.props.fetchlatestRec(uid, 1)
      }
      else if (this.props.hasQuestionierData == false) {
        this.setState({ showQuestions: true })
      }
    }
    else {
      // i know i didnt use firebase
    }

    this.props.navigation.state.params ?
      //get from navigation state
      //console.log(this.props.navigation.state.params)
      this.setState({
        displayName: userNames,
        uid: uid
      })
      :
      // get from redux store
      this.setState({
        displayName: 'NCD user'
      });
  }
  /* componentDidUpdate = () => {
    setTimeout(()=>{
      const userNames = this.props.navigation.state.params.displayName ? this.props.navigation.state.params.displayName : this.props.navigation.state.params.name
      const uid = this.props.navigation.state.params.uid ? this.props.navigation.state.params.uid : this.props.navigation.state.params.id
      if (uid) {
        console.log(uid)
        this.setState({ uid: uid },_=>{
          if (this.props.isNewUser){
            this.setState({showQuestions:true})
          }
        })
      }
    },100)
    }  */


  componentDidMount = () => {

    this.props.healthRecs.length > 0 &&
      this.setState({ heathRecords: this.props.healthRecs })
  }
  refreshScreen = () => {
    this.props.fetchlatestRec(this.state.uid, 1)
    this.props.fecthHeathRecs(this.state.uid, 1)
  }
  renderFAB = () => {
    return (
      <Provider>
        <Portal>
          <FAB.Group
            fabStyle={styles.fab}
            open={this.state.open}
            icon={'plus'}
            actions={[
              { icon: 'plus', color: '#005ecb', label: 'New Health Record', onPress: () => this._showHealthForm() },
              { icon: 'refresh', color: '#005ecb', label: 'Refresh', onPress: () => this.refreshScreen() },
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
              }
            }}
          />
        </Portal>
      </Provider>
    );
  }

  renderQuestionier = () => {
    return (
      <>
        <View style={{ height: height / 2 }}>
          <View style={{ borderTopWidth: 0.3, borderTopColor: 'red', width: width * 0.8 }}>
            <ScrollView>
              <View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>1.</Text>
                  <Text style={styles.p}>Your Gender</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <RadioButton
                    status={this.state.isMale ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ isMale: !this.state.isMale, isFemale: this.state.isMale }, () => { this.toggleGender() }) }}
                  />
                  <Text style={[styles.pt, { fontWeight: 'bold' }]}>Male</Text>
                  <RadioButton
                    status={this.state.isFemale ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ isFemale: !this.state.isFemale, isMale: this.state.isFemale }, () => { this.toggleGender() }) }}
                  />
                  <Text style={[styles.pt, { fontWeight: 'bold' }]}>Female</Text>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>2.</Text>
                  <DatePicker
                    style={{width: 250}}
                    date={this.state.dob}
                    mode="date"
                    placeholder="Select Date of Birth"
                    format="YYYY-MM-DD"
                    minDate="1930-01-01"
                    maxDate="2018-01-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 30,
                        borderRadius:5,
                        height:28,
                        borderColor:'red',
                        borderWidth:0.5
                      }
                }}
                onDateChange={(dob) => {this.setDate(dob)}}
              />
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>3.</Text>
                  <Text style={styles.p}>Non Communicable diseases(NCDs) are those that can be spread from one person to another?(Click if yes)</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <RadioButton
                    value={this.state.contigious}
                    status={this.state.contigious ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ contigious: !this.state.contigious }) }}
                  />
                  {this.state.contigious ? <Text style={styles.pt}>Yes</Text> : <Text style={styles.pt}>No</Text>}
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>4.</Text>
                  <Text style={styles.p}>How well conversant are you with diseases like Cancer, Hypertension(High blood Pressure), Diabetes melitus (High blood sugar levels)?</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    status={this.state.check1 ? 'checked' : 'unchecked'}
                    onPress={() => { this.toggleChecks('check1') }}
                  />
                  <Text style={styles.pt}>I just hear about them.</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    status={this.state.check2 ? 'checked' : 'unchecked'}
                    onPress={() => { this.toggleChecks('check2') }}
                  />
                  <Text style={styles.pt}>I have ever read about them.</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    status={this.state.check3 ? 'checked' : 'unchecked'}
                    onPress={() => { this.toggleChecks('check3') }}
                  />
                  <Text style={styles.pt}>I have been diagnosed with one of them.</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    status={this.state.check4 ? 'checked' : 'unchecked'}
                    onPress={() => { this.toggleChecks('check4') }}
                  />
                  <Text style={styles.pt}>I don't know anything about them.</Text>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>5.</Text>
                  <Text style={styles.p}>Have you ever taken measurements for any of the following? (Click to confirm)</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    value={this.state.measuredBp}
                    status={this.state.measuredBp ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ measuredBp: !this.state.measuredBp }) }}
                  />
                  <Text style={[styles.pt, { fontWeight: 'bold' }]}>Blood Pressure</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    value={this.state.measuredSugar}
                    status={this.state.measuredSugar ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ measuredSugar: !this.state.measuredSugar }) }}
                  />
                  <Text style={[styles.pt, { fontWeight: 'bold' }]}>Blood Sugar level</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <Checkbox
                    value={this.state.measuredBMI}
                    status={this.state.measuredBMI ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ measuredBMI: !this.state.measuredBMI }) }}
                  />
                  <Text style={[styles.pt, { fontWeight: 'bold' }]}>Blood mass index</Text>
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>6.</Text>
                  <Text style={styles.p}>Do you smoke cigarette?(click if Yes)</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <RadioButton
                    value={this.state.smokes}
                    status={this.state.smokes ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ smokes: !this.state.smokes }) }}
                  />
                  {this.state.smokes &&
                    <View>
                      <TextInput
                        label='Cigarates Daily'
                        keyboardType={`number-pad`}
                        value={this.state.cigarateNo}
                        style={styles.inputView2}
                        onChangeText={(cigarateNo) => this.setState({ cigarateNo })}
                      />
                    </View>
                  }
                </View>
                <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 15 }}>
                  <Text style={styles.p}>7.</Text>
                  <Text style={styles.p}>Do you drink/take Alcohol?(click if Yes)</Text>
                </View>
                <View style={{ marginLeft: 15, flexDirection: 'row' }}>
                  <RadioButton
                    value={this.state.drinkAlcohol}
                    status={this.state.drinkAlcohol ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ drinkAlcohol: !this.state.drinkAlcohol }) }}
                  />
                  {this.state.drinkAlcohol &&
                    <View>
                      <TextInput
                        label='Bottles Daily'
                        keyboardType={`number-pad`}
                        value={this.state.dailyBottles}
                        style={styles.inputView2}
                        onChangeText={(dailyBottles) => this.setState({ dailyBottles })}
                      />
                    </View>
                  }

                </View>

              </View>

            </ScrollView>
          </View>
        </View>
        <View style={{ alignSelf: 'center', paddingTop: 15, borderTopWidth: 0.3, borderTopColor: 'red', with: width }}>
          <Button
            secondary
            rounded
            style={{ width: width * 0.5 }}
            large
            caption="Submit"
            onPress={() => { this.submitQuestionaire() }}
          />
        </View>
      </>
    );
  }
  renderHealthForm = () => {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center', height: height / 2 }}>
        <ProgressSteps>
          <ProgressStep label="Blood Pressure">
            <View style={{ alignItems: 'center' }}>
              <View style={styles.formView}>
                <TextInput
                  label='Blood pressure value'
                  underlineColor={'#F44336'}
                  selectionColor={'#F44336'}
                  placeholder='120/90'
                  value={this.state.bloodPressure}
                  style={styles.inputView}
                  onChangeText={(bloodPressure) => this.setState({ bloodPressure })}
                />
              </View>

            </View>
          </ProgressStep>
          <ProgressStep label="Body Mass Index">
            <View style={{ alignItems: 'center' }}>
              <View style={styles.formView}>
                <TextInput
                  label='Weight(Kgs)'
                  underlineColor={'#F44336'}
                  selectionColor={'#F44336'}
                  value={this.state.weight}
                  keyboardType={'number-pad'}
                  style={styles.inputView}
                  onChangeText={(weight) => this.setState({ weight })}
                />
              </View>
              <View style={styles.formView}>
                <TextInput
                  label='Height(cm)'
                  underlineColor={'#F44336'}
                  selectionColor={'#F44336'}
                  value={this.state.height}
                  keyboardType={'number-pad'}
                  style={styles.inputView}
                  onChangeText={(height) => this.setState({ height })}
                />
              </View>
            </View>
          </ProgressStep>
          <ProgressStep label="Blood Glucose" onSubmit={this.onSubmit} >
            <View style={{ alignItems: 'center' }}>
              <View style={styles.formView}>
                <TextInput
                  label='Blood Glucose Level'
                  underlineColor={'#F44336'}
                  selectionColor={'#F44336'}
                  keyboardType={'number-pad'}
                  value={this.state.bloodGlucose}
                  style={styles.inputView}
                  onChangeText={(bloodGlucose) => this.setState({ bloodGlucose })}
                />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <RadioButton
                  value="mmol/l"
                  status={this.state.bloodGlucoseUnit === 'mmol/l' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ bloodGlucoseUnit: 'mmol/l' }); }}
                />
                <Text style={{ marginTop: 10 }} >mmol/l</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>

                <RadioButton
                  value="mg/dl"
                  status={this.state.bloodGlucoseUnit === 'mg/dl' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ bloodGlucoseUnit: 'mg/dl' }); }}
                />
                <Text style={{ marginTop: 10 }} >mg/dl</Text>
              </View>

            </View>
          </ProgressStep>
        </ProgressSteps>
      </View>
    )
  }
  _showHealthForm = () => {
    this.setState({ showHealthForm: true })
  }
  _openHealth = (health, type) => {
    if (this.props.healthRec === 'NO HEALTH RECORD') {
      //this.renderHealthRecForm()
      this.setState({ showHealthForm: true })
    }
    else {
      this.props.navigation.navigate({
        routeName: 'HealthDetails',
        params: { ...health },
      });
    }
  };

  onSubmit = () => {
    this.setState({
      showHealthForm: false
    }, () => {
      this.props.addNewrecord(this.state)
      setTimeout(() => {
        this.props.fetchlatestRec(this.state.uid, 1)
      }, 500)
    })

  }

  showContacts = () => {
    this.setState({
      isVisible: true
    })
  }
  renderRowOne = item => {
    return (
      <View style={styles.itemOneRow}>
        <TouchableOpacity onPress={() => this._openHealth(item, item.title)}>
          <View style={styles.itemOneContainer}>
            <View style={styles.itemOneImageContainer}>
              <ImageBackground
                style={styles.itemOneImage} source={item.image}
              >
                <View style={{ backgroundColor: item.color, height: 120, opacity: 0.5 }}></View>
              </ImageBackground>
            </View>
            <View style={styles.itemOneContent}>
              <Text style={styles.itemOneTitle} numberOfLines={1}>
                {item.title}
              </Text>
              <Text
                style={[styles.itemOneSubTitle, { color: item.color, fontWeight: 'bold' }]}
                styleName="collapsible"
                numberOfLines={3}
              >
                {item.status}
              </Text>
              <Text style={styles.itemOnePrice} numberOfLines={1}>
                {item.measure}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    )
  };

  renderBottowActionSheet = () => {
    //let facebook = <Icon family={'FontAwesome'} name={'facebook'} color={'#000000'} size={30} />
    let facebook = <Icon family={'Entypo'} name={'facebook-with-circle'} size={100} color={'#1877f2'} />
    let whatsapp = <Icon family={'FontAwesome'} name={'whatsapp'} color={'#25d366'} size={100} />
    let twitter = <Icon family={'Entypo'} name={'twitter-with-circle'} size={100} color={'#00acee'} />
    return (
      <RNBottomActionSheet.GridView visible={this.state.isVisible} title={"Awesome!"} theme={"light"} selection={3} onSelection={(index, value) => {
        if (index === 0) {
          this.appLinker(
            'https://www.facebook.com',
            'Facebook',
            'com.facebook.katana'
          )
        } else if (index == 1) {
          this.appLinker(
            'https://api.whatsapp.com/send?phone=+256706794776',
            'WhatsApp',
            'com.whatsapp'
          )
        }
        else if (index == 2) {
          this.appLinker(
            'https://twitter.com/nyikskam',
            'Twitter',
            'com.twitter.android'
          )
        }
      }}>

        <RNBottomActionSheet.GridView.Item title={"Facebook"} icon={facebook} />
        <RNBottomActionSheet.GridView.Item title={"WhatsApp"} icon={whatsapp} />
        <RNBottomActionSheet.GridView.Item title={"Twitter"} icon={twitter} />
      </RNBottomActionSheet.GridView>
    )
  }
  renderRowTwo = item => (
    <TouchableOpacity
      style={styles.itemTwoContainer}
      onPress={() => this._openHealth(item, 'glucose')}
    >
      <View style={styles.itemTwoContent}>
        <Image style={styles.itemTwoImage} source={item.image} />
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 1 }}
          colors={item.colors}
          style={styles.itemTwoOverlay}
        />
        <Text style={styles.itemTwoTitle}>{item.title}</Text>
        <Text style={styles.itemTwoPrice}>{item.status}</Text>
        <Text style={styles.itemTwoPrice}>{item.measure}</Text>
      </View>
    </TouchableOpacity>
  );

  render() {
    setTimeout(() => {
      return null //delay
    }, 100)
    const healthRec = this.props.healthRec
    let listItems = [];
    let glucoseBar;
    if (Array.isArray(healthRec) && healthRec.length > 0) {
      listItems = healthRec
      glucoseBar = healthRec[2]
    }
    else {
      listItems = listData
      glucoseBar = bloodGlucoseBar[0]
    }
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="red" barStyle="light-content" />
        <ImageBackground
          resizeMode="cover"
          source={require('../../../assets/images/health_8.png')}
          style={[styles.section, styles.header]}
        >
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={styles.title}>Hello!</Text>
            <View>
              <Text style={styles.position}>{this.state.displayName}</Text>
              <Text style={styles.company}>Together we can stop that ticking NCD bomb inside you!</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>

            <Button
              secondary
              rounded
              small
              caption="About Us"
              onPress={() => { }}
            />
            {this.renderBottowActionSheet()}
            <Button
              rounded
              bordered
              small
              style={{ marginLeft: 20 }}
              caption="Follow"
              onPress={() => { this.showContacts() }}
            />

          </View>
        </ImageBackground>
        <View style={styles.section}>
          <LinearGradient
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 0 }}
            colors={[colors.profileGradientStart, colors.profileGradientEnd]}
            style={styles.quickFacts}
          >
          </LinearGradient>
          <ScrollView style={{ paddingHorizontal: 10 }}>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', flex: 1, justifyContent: 'center' }}>
              {this.renderRowOne(listItems[0])}
              {this.renderRowOne(listItems[1])}
            </View>
            <View style={{ paddingHorizontal: 10 }}>{this.renderRowTwo(glucoseBar)}</View>

            <View style={{
              margin: 10,
              paddingHorizontal: 10, backgroundColor: '#e1f5fe', borderRadius: 10
            }}>
              {
                this.props.healthRecs.length > 0 ?
                  <>
                    <PureChart width={width}  data={this.props.healthRecs} type='line' />
                    <View style={{ paddingHorizontal: 10, margin: 10, flexDirection: 'row' }}>
                      <View style={{ backgroundColor: '#ab000d', borderRadius: 5, marginRight: 10 }}>
                        <Text style={[styles.p, { color: 'white', margin: 5, fontSize: 10 }]}>Glucose</Text>
                      </View>
                      <View style={{ backgroundColor: '#0031ca', borderRadius: 5, marginRight: 10 }}>
                        <Text style={[styles.p, { color: 'white', margin: 5, fontSize: 10 }]}>Body Mass Index</Text>
                      </View>
                      <View style={{ backgroundColor: '#00701a', borderRadius: 5, marginRight: 10 }}>
                        <Text style={[styles.p, { color: 'white', margin: 5, fontSize: 10 }]}>Pressure(Systolic)</Text>
                      </View>
                      <View style={{ backgroundColor: '#ff5722', borderRadius: 5, marginRight: 10 }}>
                        <Text style={[styles.p, { color: 'white', margin: 5, fontSize: 10 }]}>Pressure(Diastolic)</Text>
                      </View>
                    </View>
                  </>
                  : null
              }
            </View>
          </ScrollView>
          {this.renderFAB()}
        </View>

        {/* <ActivityLoader loading={this.props.fetching } /> */}
        <ActivityLoader loading={this.props.submitting} />
        {this.props.adding ? showDialogProgress('Submitting') : hideDialogProgress()}

        {this.props.healthError &&
          <ErrorDisplay errorMsg={this.props.healthError} reset={this.resetHeath} />
        }
        {this.props.submitError &&
          <ErrorDisplay errorMsg={this.props.submitError} reset={this.resetSubmit} />
        }
        {this.props.errorSubmitting &&
          <ErrorDisplay errorMsg={this.props.errorSubmitting} reset={this.resetInitialInfoData} />
        }
        {this.props.submitted && <SuccessAlert reset={this.resetSubmit} successMsg={'Congs! Your healthy record has been submitted, Now check the three health categories for recommendations'} />}
        {this.props.doneSubmitting && <SuccessAlert reset={this.resetInitialInfoData} successMsg={'Congs! Questionaire Submitted Successfully'} />}

        <AwesomeAlert
          show={this.state.showHealthForm}
          showProgress={false}
          customView={this.renderHealthForm()}
          closeOnTouchOutside={false}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          confirmButtonColor="#DD6B55"
          cancelText="No, cancel"
          confirmText="PROCEED LATER"
          onConfirmPressed={() => {
            this.hideAlert();
          }}
        />
        {(this.props.hasQuestionierData === false) &&
          <AwesomeAlert
            show={true}
            title={<Text style={{fontSize:12,fontWeight:'bold'}}>Please Share What You Know About NCDs</Text>}
            showProgress={false}
            customView={this.renderQuestionier()}
            closeOnTouchOutside={false}
            closeOnHardwareBackPress={true}
            showCancelButton={false}
            showConfirmButton={false}
          />}

      </View>
    );
  }
}
export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  itemOneSubTitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 13,
    color: '#B2B2B2',
    marginVertical: 3,
  },
  itemOnePrice: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  itemOneContent: {
    marginTop: 5,
    marginBottom: 5,
  },
  itemOneTitle: {
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
  },
  p: {
    marginVertical: 5,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
  },
  pt: {
    marginVertical: 10,
    lineHeight: 20,
    letterSpacing: 0,
    color: colors.gray,
  },
  itemOneRow: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    justifyContent: 'space-around',
    marginTop: 10,
  },
  itemOneImageContainer: {
    borderRadius: 3,
    overflow: 'hidden',
  },
  itemOneImage: {
    height: 120,
    width: Dimensions.get('window').width / 2 - 45,
    borderRadius: 10,
  },
  itemOneContainer: {
    width: Dimensions.get('window').width / 2 - 25,
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: colors.primary,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    padding: 8,
  },
  header: {
    flex: 1,
    padding: 20,
  },
  section: {
    flex: 3,
    position: 'relative',
  },
  title: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 25,
    letterSpacing: 0.04,
    marginBottom: 10,
  },
  lightText: {
    color: colors.white,
  },
  quickFacts: {
    height: 30,
    flexDirection: 'row',
  },
  quickFact: {
    flex: 1,
  },
  infoSection: {
    flex: 1,
  },
  infoRow: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
    flexDirection: 'row',
  },
  hr: {
    borderBottomColor: '#e3e3e3',
    borderBottomWidth: 1,
    marginLeft: 20,
  },
  infoIcon: {
    marginRight: 20,
  },
  bottomRow: {
    height: 80,
    flexDirection: 'row',
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  position: {
    color: colors.white,
    fontFamily: fonts.primaryLight,
    fontSize: 16,
    marginBottom: 3,
  },
  company: {
    color: colors.white,
    fontFamily: fonts.primaryRegular,
    fontSize: 16,
  },
  quickInfoItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  quickInfoText: {
    color: colors.white,
    fontFamily: fonts.primaryRegular,
  },
  bottomImage: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemTwoContainer: {
    paddingBottom: 10,
    backgroundColor: colors.whiteTwo,
    marginVertical: 5,
  },
  itemTwoContent: {
    padding: 20,
    marginTop: 20,
    position: 'relative',
    marginHorizontal: Platform.OS === 'ios' ? -15 : 0,
    height: 150,
  },
  itemTwoTitle: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 20,
  },
  itemTwoSubTitle: {
    color: colors.white,
    fontFamily: fonts.primaryRegular,
    fontSize: 15,
    marginVertical: 5,
  },
  itemTwoPrice: {
    color: colors.white,
    fontFamily: fonts.primaryBold,
    fontSize: 20,
  },
  itemTwoImage: {
    position: 'absolute',
    top: 0,
    width: width - 40,
    marginRight: 10,
    height: 150,
    resizeMode: 'stretch',
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 10
  },
  itemTwoOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    opacity: 0.5,
    borderRadius: 10
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  formView: {
    paddingHorizontal: 20,
    marginTop: 5,
    justifyContent: 'space-between'
  },
  inputView: {
    padding: 10,
    backgroundColor: 'white',
    marginVertical: 5,
    height: 50,
    width: width * 0.7,
    borderWidth: 0.5,
    borderColor: "#F44336",
    elevation: 1
  },
  inputView2: {
    padding: 2,
    backgroundColor: 'white',
    height: 52,
    width: width * 0.6,
  },
  fab: {
    backgroundColor: '#004ecb',
  },

});
