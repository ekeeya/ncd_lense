import { compose, withState } from 'recompose';
import { connect } from 'react-redux';
import HomeScreen from './HomeView';
import { fetchUserHealthRec,resetHealthState } from '../../redux/actions/latestUserHealthRec'
import { addNewHealthRecord, resetHealthAddState } from '../../redux/actions/addHealthRecord'
import { resetQuestionaireCheck } from '../../redux/actions/checkQuestionier';
import { addQuestionaire } from '../../redux/actions/addQuestionaire';
import { resetQuestionaireAddState } from '../../redux/actions/addQuestionaire'
import { getHealthRecords } from '../../redux/actions/getHealthRecords';

function mapStateToProps(state){
    return{
        user:state.signInWithFirebase.user,
        isNewUser:state.signInWithFirebase.isNewUser,
        provider:state.signInWithFirebase.provider,
        healthRec:state.UserhealthRecord.healthRec,
        fetching:state.UserhealthRecord.fetching,
        healthError:state.UserhealthRecord.healthError,
        adding:state.addNewHealthRec.adding,
        submitted:state.addNewHealthRec.submitted,
        submitError:state.addNewHealthRec.submitError,
        newRec:state.addNewHealthRec.newRec,
        checking: state.checkQuestionier.checking,
        hasQuestionierData:state.checkQuestionier.hasQuestionierData,
        checkError:state.checkQuestionier.checkError,
        submitting:state.addQuestionaire.submitting,
        doneSubmitting:state.addQuestionaire.doneSubmitting,
        errorSubmitting:state.addQuestionaire.errorSubmitting,
        healthRecs:state.fetchHealthRecords.healthRecs
        }
}

 function mapDispatchToProps(dispatch){
     return{
         fetchlatestRec: (userId,fromFirebase)=>{dispatch(fetchUserHealthRec(userId,fromFirebase))},
         resetHealth :()=> dispatch(resetHealthState()),
         addNewrecord:(state)=>dispatch(addNewHealthRecord(state)),
         resetHealthAdd:()=>dispatch(resetHealthAddState()),
         resetQuestionaireAdd:()=>dispatch(resetQuestionaireAddState()),
        // checkQuestionier:(userId,isFirebase)=>dispatch(checkQuestionierData(userId,isFirebase)),
         addInitialInfo:(state,fromFirebase) => dispatch(addQuestionaire(state,fromFirebase)),
         fecthHeathRecs :(userID,isFirebase)=>dispatch(getHealthRecords(userID,isFirebase)),
         resetQuestionierCheck:()=>dispatch(resetQuestionaireCheck())
     }
 }


export default compose(connect(mapStateToProps,mapDispatchToProps))(HomeScreen);

