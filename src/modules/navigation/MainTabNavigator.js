/* eslint-disable import/no-unresolved */
import React from 'react';
import { Image, View, StyleSheet, Text } from 'react-native';
import { Icon } from 'native-base';
import { createBottomTabNavigator } from 'react-navigation';

import { colors, fonts } from '../../styles';

import HomeScreen from '../home/HomeViewContainer';
import CalendarScreen from '../calendar/CalendarViewContainer';
import ChatScreen from '../chat/ChatViewContainer';
import SettingsScreen from '../settings/SettingsViewContainer'
import FeedBackScreen from '../feedback/FeedBackViewContainer'
import NCDList from '../NCDs/NCDListViewContainer'

const hederBackground = require('../../../assets/images/topBarBg.png');

const styles = StyleSheet.create({
  tabBarItemContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  tabBarIcon: {
    fontSize: 25,
    color: colors.darkGray
  },
  tabBarIconFocused: {
    color: colors.primary,
    fontSize: 25
  },
  headerContainer: {
    height: 70,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 10,
  },
  headerImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    height: 70,
    resizeMode: 'cover',
  },
  headerCaption: {
    fontFamily: fonts.primaryRegular,
    color: colors.white,
    fontSize: 18,
  },
});

export default createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        header: null,
      },
    },
    Calendar: {
      screen: CalendarScreen,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>Calendar</Text>
          </View>
        ),
      },
    },
    NCDs:{
      screen:NCDList,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>NCDs</Text>
          </View>
        ),
      },
    },/* 
    Forum: {
      screen: ChatScreen,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>Forum</Text>
          </View>
        ),
      },
    }, */
    Feedback: {
      screen: FeedBackScreen,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>Feedback</Text>
          </View>
        ),
      },
    },
    
   
    
    /* Grids: {
      screen: GridsScreen,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>Grids</Text>
          </View>
        ),
      },
    },
    Pages: {
      screen: PagesScreen,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>Pages</Text>
          </View>
        ),
      },
    },
    Components: {
      screen: ComponentsScreen,
      navigationOptions: {
        header: (
          <View style={styles.headerContainer}>
            <Image style={styles.headerImage} source={hederBackground} />
            <Text style={styles.headerCaption}>Components</Text>
          </View>
        ),
      },
    },*/
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      // eslint-disable-next-line react/prop-types
      tabBarIcon: ({ focused }) => {
        const { routeName, tintColor } = navigation.state;
        let iconName;
        let type;
        switch (routeName) {
          case 'Home':
            iconName = 'home';
            type = "Entypo"
            break;
          case 'Calendar':
            iconName = 'calendar';
            type = "AntDesign"
            break;
          case 'Forum':
            iconName = 'chat';
            type = "Entypo"
            break;
           case 'NCDs':
            iconName = 'book-medical';
            type = "FontAwesome5"
            break;  
          case 'Feedback':
            iconName = 'feedback';
            type = "MaterialIcons"
            break;
          /* case 'Grids':
            iconName = iconGrids;
            break;
          case 'Pages':
            iconName = iconPages;
            break;
          case 'Components':
            iconName = iconComponents;
            break; */
          default:
            iconName = '';
        }
        return (
          <View style={styles.tabBarItemContainer}>
            <Icon type={type} name={iconName} style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]} />
            {/* <Image
              resizeMode="contain"
              source={iconSource}
              style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
            /> */}
          </View>
        );
      },
    }),
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions: {
      showLabel: true,
      style: {
        backgroundColor: colors.background,
      },
      labelStyle: {
        color: colors.grey,
      },
    },
  },
);
