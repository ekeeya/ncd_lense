import React from 'react';
import { Image, TouchableOpacity, Dimensions,View,Text } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import HealthScreen from '../health/healthViewContainer';
import NcdDetailScreen from '../../modules/NCDs/viewNCD'
import ChatScreen from '../chat/ChatViewContainer';
import MessagesScreen from '../chat/MessagesViewContainer';
import AuthScreen from '../auth/AuthViewContainer';
import FastImage from 'react-native-fast-image'

import { colors, fonts } from '../../styles';

const { width } = Dimensions.get('window');

const headerBackground = require('../../../assets/images/topBarBg.png');
const logoImage = require('../../../assets/images/logo_1.png')

const stackNavigator = createStackNavigator(
  {
    Auth: {
      screen: AuthScreen,
      navigationOptions: {
        header: null,
      },
    },
    Main: {
      screen: MainTabNavigator,
      navigationOptions: () => ({
        title: 'NCD Lens',
        headerLeft:null,
        headerRight: <View style={{padding:10,marginRight:10}}>
          <FastImage
            style={{ width: 50, height: 50 }}
            source={logoImage}
            resizeMode={FastImage.resizeMode.contain}
        />
        </View>,
        headerBackground: (
          <Image
            style={{
              flex: 1,
              width
            }}
            source={headerBackground}
            resizeMode="cover"
          />
        ),
      }),
    },
    NcdDisease: {
      screen: NcdDetailScreen,
      navigationOptions: {
        title: 'Disease Details',
      },
    },
    HealthDetails: {
      screen: HealthScreen,
      navigationOptions: {
        title: 'Health',
      },
    },
    Chat: {
      screen: ChatScreen,
      navigationOptions: {
        title: 'Chat',
      },
    },
    Messages: {
      screen: MessagesScreen,
      navigationOptions: {
        title: 'Messages',
      },
    },
  },
  {
    defaultNavigationOptions: () => ({
      titleStyle: {
        fontFamily: fonts.primaryLight,
      },
      headerStyle: {
        backgroundColor: colors.primary,
        borderBottomWidth: 0,
      },
      headerBackground: (
        <Image
          style={{ flex: 1 }}
          source={headerBackground}
          resizeMode="cover"
        />
      ),
      headerTitleStyle: {
        color: colors.white,
        fontFamily: fonts.primaryRegular,
      },
      headerTintColor: '#222222',
      headerLeft: props => (
        <TouchableOpacity
          onPress={props.onPress}
          style={{
            paddingLeft: 25,
          }}
        >
          <Image
            source={require('../../../assets/images/icons/arrow-back.png')}
            resizeMode="contain"
            style={{
              height: 20,
            }}
          />
        </TouchableOpacity>
      ),
    }),
  },
);

export default createAppContainer(stackNavigator);
