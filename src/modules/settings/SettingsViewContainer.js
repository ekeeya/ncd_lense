// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import SettingsScreen from './SettingsView';

export default compose(connect())(SettingsScreen);
