import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 15000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


export const getWHORecommendation = (
    gender,age,systolic,isSmoker,isDiabetic
) => {
    return (dispatch) => {
        dispatch({ type: 'WHO_START' })
        httpClient.post(`${ROOT_URL}/user/get-who-prediction`,
            {
                gender:gender,
                age:age,
                systolic:systolic,
                isSmoker:isSmoker,
                isDiabetic:isDiabetic
            }).then((response) => {
                console.log(response)
                if (response.data.message === "success") {
                    dispatch({
                        type: 'WHO_SUCCESS',
                        recommendation:response.data
                    })
                } else{
                    dispatch({ type: 'WHO_ERROR', error: response.data.message })
                }
            }).catch((error) => {
                console.log(error.message)
                dispatch({ type: 'WHO_ERROR', error: error.message })
            })
    }
}

