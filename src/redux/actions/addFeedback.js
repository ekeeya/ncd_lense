import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


export const addFeedback = (state) => {
    const { userId, rating, message} = state
    return  (dispatch) => {
        dispatch({type:'START_RATE'})
        httpClient.post(`${ROOT_URL}/add-feedback`,
                {
                    userId:userId,
                    fromFirebase:1,
                    ratings: rating,
                    message: message,

                }).then((response)=>{
                    if(response.data.message == 'Success'){
                        dispatch({
                            type: 'RATE_SUCCESS'
                        })
                    }
                }).catch((error)=>{
                    dispatch({type:'RATE_ERROR',error:error.message})
                })
    }

}

export const resetRating = ()=>{
    return(dispatch)=>{
        dispatch({type:'RESET_RATING'})
    }
}