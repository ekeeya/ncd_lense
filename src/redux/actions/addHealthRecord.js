import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL



calculateBMI = (height, weight) => {
    const h = height
    const w = weight
    const hm = (h / 100);
    const BMI = (weight / (hm * hm))
    return Math.round(BMI)
  }


export const addNewHealthRecord = (state) => {
    const {bloodPressure, height, weight, bloodGlucose, bloodGlucoseUnit, userId, uid } = state
    let fromFirebase;
    let clientId;
    uid ? fromFirebase = 1  : fromFirebase = 0
    uid ? clientId = uid : clientId = userId
    return  (dispatch) => {
        
        dispatch({type:'ADD_START'})
        httpClient.post(`${ROOT_URL}/add-health-record`,
                {
                    userId:clientId,
                    fromFirebase:fromFirebase,
                    height: height,
                    weight: weight,
                    bloodPressure: bloodPressure,
                    bloodGlucose:bloodGlucoseUnit=="mmol/l" ?(bloodGlucose*18):bloodGlucose,
                    bloodGlucoseUnit: "mg/dl",
                    bmi:calculateBMI(parseFloat(height),parseFloat(weight)),

                }).then((response)=>{
                    if(response.data.message == 'NEW'){
                        dispatch({
                            type: 'ADD_SUCCESS',
                            record:response.data
                        })
                    }
                }).catch((error)=>{
                    dispatch({type:'ADD_ERROR',error:"Couldn't submit your health record, Please ensure you have an internet connection and try again"})
                })
    }

}

export const resetHealthAddState = ()=>{
    return(dispatch)=>{
        dispatch({type:'ADD_RESET'})
    }
}