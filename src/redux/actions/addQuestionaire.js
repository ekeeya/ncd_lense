import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL
import { checkQuestionierData } from '../../redux/actions/checkQuestionier';

export const addQuestionaire = (state, fromFirebase) => {
    let userID;
    console.log(state)
    const { measuredBp, dob, gender, measuredSugar, measuredBMI, smokes, cigarateNo, drinkAlcohol,
        contigious, ncdKnowLevel, userId, uid, dailyBottles } = state
    if (fromFirebase === 1) {
        userID = uid
    }
    else {
        userID = userId
    }
    return (dispatch) => {
        dispatch({ type: 'ADD_QUESTIONAIRE' })
        httpClient.post(`${ROOT_URL}/add-questionier`,
            {
                userID: userID,
                fromFirebase: fromFirebase,
                gender: gender,
                dob: dob,
                contigious: contigious,
                ncdKnowLevel: JSON.stringify(ncdKnowLevel),
                bpTest: measuredBp,
                sugarTest: measuredSugar,
                bmiTest: measuredBMI,
                smoke: smokes,
                drinkAlcohol: drinkAlcohol,
                dailyBottles: parseInt(dailyBottles),
                cigarateNo: parseInt(cigarateNo)
            }).then((response) => {
                if (response.data.message == 'SUCCESS') {
                    dispatch(checkQuestionierData(userID, 1))
                    dispatch({
                        type: 'ADD_QUESTIONAIRE_SUCCESS',
                    })
                }
            }).catch((error) => {
                dispatch({ type: 'ADD_QUESTIONAIRE_ERROR', error: error.message })
            })
    }

}

export const resetQuestionaireAddState = ()=>{
    return(dispatch)=>{
        dispatch({type:'RESET_QUESTIONAIRE_STATE'})
    }
}