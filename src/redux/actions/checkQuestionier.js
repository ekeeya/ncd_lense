import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL

export const checkQuestionierData = (userId, fromFirebase) => {
    return (dispatch) => {
        dispatch({ type: 'CHECK_START' })
        httpClient.post(`${ROOT_URL}/check_questionier`,
            {
                userId: userId,
                fromFirebase: fromFirebase
            }).then((response) => {
                if (response.data.message === "EXISTS") {
                    dispatch({
                        type: 'CHECK_SUCCESS',
                        val: true,
                        data:response.data.info
                    })
                } else {
                    dispatch({
                        type: 'CHECK_SUCCESS',
                        val: false
                    })
                }
            }).catch((error) => {
                console.log(error)
                dispatch({ type: 'CHECK_ERROR', error: error.message })
            })
    }
}

export const resetQuestionaireCheck =()=>{
    return (dispatch) => {
        dispatch({ type: 'CHECK_RESET' })
    }
}