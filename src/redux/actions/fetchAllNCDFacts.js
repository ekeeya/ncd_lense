import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 15000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


export const getNCDFacts = () => {
    return (dispatch) => {
        httpClient.get(`${ROOT_URL}/ncd-facts`)
        .then((response) => {
                dispatch({ type: 'GOT_NCD_FACTS',facts:response.data })
            }).catch((error) => {
                dispatch({ type: 'GOT_NCD_ERROR', error: error.message })
            })
    }
}

