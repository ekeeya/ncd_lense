import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


const dateFormater = (dateString)=>{
    let newDate = new Date(dateString)
    let year = newDate.getFullYear()
    let month = newDate.getMonth()+1
    let day = newDate.getDate()
    return year + "-"+("0" + month).slice(-2) +"-"+("0" + day).slice(-2)    
    }
const arrayForm = (data) => {
    console.log(data)
        let myArray = data
        let newDataArray = []

        let GlucoseLevelDataArray = []
        let bmiDataArray = []
        let systoricArray = []
        let diastoricArray = []

        for (let i = 0; i < myArray.length; i++) {
            GlucoseLevelDataArray.push({ 'x': dateFormater(myArray[i].created_at), 'y': Math.round(myArray[i].blood_glucose_level) })
        }
        for (let i = 0; i < myArray.length; i++) {
            bmiDataArray.push({ 'x': dateFormater(myArray[i].created_at), 'y': parseInt(myArray[i].bmi) })
        }
        for (let i = 0; i < myArray.length; i++) {
            systoricArray.push({ 'x': dateFormater(myArray[i].created_at), 'y': parseInt(myArray[i].systolic) })
        }

        for (let i = 0; i < myArray.length; i++) {
            diastoricArray.push({ 'x': dateFormater(myArray[i].created_at), 'y': parseInt(myArray[i].diastolic) })
        }
        let GlucoseLevelData = { 'seriesName': 'GlucoseLevel', 'color': '#ab000d', 'data': GlucoseLevelDataArray }
        let bmiData = { 'seriesName': 'BMI', 'color': '#0031ca', 'data': bmiDataArray }
        let systoricData = { 'seriesName': 'systolic', 'color': '#00701a', 'data': systoricArray }
        let diastoricData = { 'seriesName': 'diastolic', 'color': '#ff5722', 'data': diastoricArray }
        newDataArray.push(GlucoseLevelData, bmiData, systoricData, diastoricData)
        console.log(newDataArray)
        return newDataArray
    }

export const getHealthRecords = (user_id, fromFirebase) => {
        
    return (dispatch) => {
        dispatch({ type: 'START_REC_CHECK' })
        httpClient.post(`${ROOT_URL}/my-healthy-recs`,
            {
                userId: user_id,
                fromFirebase: fromFirebase

            }).then((response) => {
                console.log(response)
                if(response.data.length > 0){
                    dispatch({ type: 'REC_CHECK_SUCCESS', records: arrayForm(response.data)})
                }
                else{
                    dispatch({ type: 'REC_CHECK_SUCCESS', records: response.data})
                }
            }).catch((error) => {
                dispatch({ type: 'REC_CHECK_ERROR', error: error.message })
            })
    }
}