import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 15000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


export const getNCData = () => {
    return (dispatch) => {
        dispatch({ type: 'GET_NCD_START' })
        httpClient.get(`${ROOT_URL}/get/ncds`)
        .then((response) => {
                dispatch({ type: 'GET_NCD_SUCCESS',data:response.data })
            }).catch((error) => {
                dispatch({ type: 'GET_NCD_ERROR', error: error.message })
            })
    }
}

