import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


export const getRecommendation = (category, status) => {
    return (dispatch) => {
        dispatch({ type: 'REC_START' })
        httpClient.post(`${ROOT_URL}/get-recommendations`,
            {
                category: category,
                status: status

            }).then((response) => {
                dispatch({ type: 'REC_SUCCESS', rec: response.data[0]})
            }).catch((error) => {
                dispatch({ type: 'REC_ERROR', error: error.message })
            })
    }
}