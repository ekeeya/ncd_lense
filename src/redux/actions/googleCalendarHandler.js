import RNCalendarEvents from 'react-native-calendar-events';
import * as AddCalendarEvent from 'react-native-add-calendar-event';
const moment = require('moment')
const STATUS = ['denied', 'restricted', 'authorized', 'undetermined']

// this will fetch events from Jan 1 of the respective year to now()



export const fetchAllEvents = () => {
    const year = new Date().getFullYear()
    const newYearDay = `${year}-01-01T00:00:00.000Z`
    const today = new Date().toISOString()
    const currentTime = moment()
    const futureMonth = currentTime.add(10, 'M');
    const endDate = new Date(futureMonth._d).toISOString()
    return async (dispatch) => {
        // Lets first check status
        let eventItems = {};
        let markedDates = {};
        let color = ''
        let status = ''
        RNCalendarEvents.authorizeEventStore();
        RNCalendarEvents.authorizationStatus()
            .then((status) => {
                if (status == STATUS[2]) {
                    dispatch({ type: 'EVENTS_FETCH_START' })
                    RNCalendarEvents.fetchAllEvents(
                        newYearDay,
                        endDate
                    ).then((events) => {
                        events.forEach(event => {
                            const eventDate = new Date(event.startDate).toISOString().split('T')[0];
                            const eventComparedate = new Date(event.endDate)
                            const nowDate = new Date()
                            if (eventComparedate < nowDate) {
                                color = '#00701a',
                                    status = 'Completed'
                            }
                            else if (eventComparedate == nowDate) {
                                color = "#32cb00",
                                    status = 'Active today'
                            }
                            else {
                                color = "#005ecb",
                                    status = 'Pending'
                            }

                            eventItems[eventDate] = [];
                            markedDates[eventDate] = { selected: true, marked: true };
                            eventItems[eventDate].push({
                                id: event.id,
                                name: event.title,
                                color: color,
                                startDate: event.startDate,
                                endDate: event.endDate,
                                status: status
                            })
                        });
                        dispatch({ type: 'EVENTS_FETCHED', events: eventItems, markedDates: markedDates })
                    }).catch((error) => {
                    })
                }
                else {
                    RNCalendarEvents.authorizeEventStore();
                }
            })

    }
}



export const addReminder = () => {
    const today = new Date().toISOString()
    const now = new Date()
    return async (dispatch) => {
        
        const eventConfig = {
            title: `${moment.monthsShort(now.getMonth() + 1)} ${now.getFullYear()} Health Check up`,
            startDate: today,
            endDate: today,
            allDay: true,
            notes: `I need to do a general health checkup`
        };
        AddCalendarEvent.presentEventCreatingDialog(eventConfig)
            .then((eventInfo) => {
                null
            })
            .catch((error) => {
                // handle error such as when user rejected permissions
                console.warn(error);
            });
    }
}

export const editEvent = (id)=>{
    return async(dispatch)=>{
        AddCalendarEvent.presentEventEditingDialog({
            eventId:id
        }).then(()=>{
        }).catch((err)=>{
        })
    }
}


export const removeEvent = (id) => {
    return async (dispatch) => {
        dispatch({ type: 'EVENT_REMOVE_START' })
        RNCalendarEvents.removeEvent(id)
            .then((success) => {
                dispatch({ type: 'EVENT_REMOVE_SUCCESS' })
            }).catch((err) => {
                dispatch({ type: 'EVENT_REMOVE_FAIL', error: err })
            })
    }
}

export const hideDialog = (id) => {
    return async (dispatch) => {
        dispatch({ type: 'HIDE_DIALOG' })

    }
}