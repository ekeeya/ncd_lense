import axios from 'axios'
import { ROOT_URL } from '../../utils/index'

const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL


export const fetchUserHealthRec = (userId,fromFirebase) => {
    return  (dispatch) => {
        dispatch({type:'HEALTH_FETCH_START'})
        httpClient.post(`${ROOT_URL}/user-health-rec`,
                {
                    userId:userId,
                    fromFirebase:fromFirebase
                }).then((response)=>{
                    if(response.data.message == 'HAS_REC'){
                        const latestHealthData = [
                            rateBloodPressure(parseInt(response.data.systolic),parseInt(response.data.diastolic), response.data.client_id),
                            rateBMIStatus(parseInt(response.data.bmi),response.data.client_id),
                            rateGlucoseLevel(response.data.blood_glucose_level,'Random Blood Glucose',response.data.client_id)
                        ]
                        dispatch({
                            type: 'HEALTH_FETCH_SUCCESS',
                            record:latestHealthData
                        })
                        
                    }
                    else{
                        dispatch({
                            type: 'HEALTH_FETCH_SUCCESS',
                            record:response.data.message
                        })
                    }
                }).catch((error)=>{
                    dispatch({type:'HEALTH_FETCH_ERROR',error:`Couldn't Fetch Your Health Record\n${error.message}`})
                })
    }
}


export const resetHealthState = ()=>{
    return(dispatch)=>{
        dispatch({type:'HEALTH_FETCH_RESET'})
    }
}

const rateBMIStatus = (BMI,client_id) => {
    let color;
    let statusTitle;
    let iconName;
    if (BMI <= 18.4) {
        color='#B71C1C'
        statusTitle = "BAD(Under weight)"
        iconName = 'times'
    }
    else if (BMI <= 24.9) {
        color='#43A047'
        statusTitle = "GOOD:(Normal Weight)"
        iconName = 'check'

    }
    else if (BMI <= 30.0) {
        color="#FFD54F"
        statusTitle = "Moderate:Over weight"
        iconName = 'exclamation-triangle'
    }
    else {
        color='#B71C1C'
        statusTitle = "BAD: Obese"
        iconName = 'times'
    }
    let options = {
        client_id:client_id,
        color:color,
        title:"BODY MASS INDEX",
        measure:`${BMI} kgm^2`,
        status: statusTitle,
        iconName: iconName,
        image:require('../../../assets/images/health_fat.jpg')
    }
    return options
}

const rateGlucoseLevel = (glucoseLevel, glucose_form,client_id) => {
    const formR = "Random Blood Glucose"
    const formF = "Fasting Blood Glucose"
    let color;
    let status;
    let GiconName;
    if ((glucose_form == formR && glucoseLevel < 140) || glucose_form == formF && glucoseLevel <= 100) {
        color=['#43A047','#2E7D32']
        status = "GOOD: Normoglycemic"
        GiconName = 'check'
    }
    else if ((glucose_form == formR && glucoseLevel <= 199) || glucose_form == formF && glucoseLevel <= 125) {
        color=["#F4FF81","#FFD54F"]
        status = "MODERATE:Glucose Intolerant"
        GiconName = 'exclamation-triangle'
    }
    else if ((glucose_form == formR && glucoseLevel > 199) || glucose_form == formF && glucoseLevel > 125) {
        color=['#E53935','#B71C1C']
        status = "BAD:Hyperglycemic"
        GiconName = 'times'
    }
    else if ((glucose_form == formR && glucoseLevel < 70) || glucose_form == formF && glucoseLevel < 70) {
        color=['#E53935','#B71C1C']
        status = "BAD:Hypoglycemic"
        GiconName = 'times'
    }

    let Goptions = {
        client_id:client_id,
        title: 'BODY GLUCOSE',
        status: status,
        measure: `${Math.round(glucoseLevel)} mg/dl`,
        image:require('../../../assets/images/glucose.jpg'),
        colors: color,
    }
    return Goptions
}

const rateBloodPressure = (upper, lower,client_id) => {
    const u = upper
    const l = lower
    let color;
    let BPIconName;
    let BPStatusTitle;
    if (u < 90 && l < 70) {
        color='#B71C1C'
        BPStatusTitle = "BAD:Hypotension"
        BPIconName = 'times'
    }
    else if ((u >= 90 && u <= 120) && (l >= 50 && l <= 90)) {
        color='#43A047'
        BPStatusTitle = "GOOD: Normal Pressure"
        BPIconName = 'check'
    }
    else if ((u >= 121 && u < 140) && (l >= 60 && l < 91)) {
        color="#FFD54F"
        BPStatusTitle = "MODERATE: Pre-hypertensive"
        BPIconName = 'exclamation-triangle'
    }
    else if (u >= 140 && l >= 90) {
        color='#B71C1C'
        BPStatusTitle = "BAD: Hypertensive"
        BPIconName = 'times'
    }
    else if (u > 140 && l < 70) {
        color='blue'
        BPStatusTitle = "Isolated systolic hyper"
        BPIconName = 'times'
    }
    else {
        color='blue'
        BPStatusTitle = "UnKnown"
        BPIconName = 'help'
    }
     const options = {
        client_id:client_id,
        color:color,
        status: BPStatusTitle,
        title:'BLOOD PRESSURE',
        image: require('../../../assets/images/bp_doc.jpg'),
        measure: `${upper}/${lower}`,
        BPIconName: BPIconName
    }
    return options
}

    