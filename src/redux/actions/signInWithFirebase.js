import { GoogleSignin } from 'react-native-google-signin';
import { AccessToken, LoginManager } from 'react-native-fbsdk';
import { firebase } from '@react-native-firebase/auth';
import { checkQuestionierData } from '../../redux/actions/checkQuestionier';
import { getHealthRecords } from '../../redux/actions/getHealthRecords';
import { fetchUserHealthRec } from '../../redux/actions/latestUserHealthRec'
import { Alert } from 'react-native'
import axios from 'axios'
import { ROOT_URL } from '../../utils/index'
const httpClient = axios.create();
httpClient.defaults.timeout = 5000; // to be extended to 10s
httpClient.defaults.baseURL = ROOT_URL
//import {showDialogProgress, hideDialogProgress} from '../../components/utils'

export const signinWithGoogle = () => {
    return async (dispatch) => {
        try {

            /* await GoogleSignin.configure({
                scopes: ["https://www.googleapis.com/auth/user.birthday.read",
                    "https://www.googleapis.com/auth/user.phonenumbers.read"]
            }); */
            await GoogleSignin.configure();
            // Login with Google
            dispatch({ type: 'AUTH_START' })

            await GoogleSignin.signIn();
            const tokens = await GoogleSignin.getTokens()
            const { idToken, accessToken } = tokens

            // Create a Firebase credential with the tokens.
            const credential = firebase.auth.GoogleAuthProvider.credential(idToken, accessToken);
            // login with credentials
            const firebaseUser = await firebase.auth().signInWithCredential(credential);
            if (firebaseUser.additionalUserInfo.isNewUser == true){
                const {email, family_name, given_name, id} = firebaseUser.additionalUserInfo.profile
                httpClient.post(`${ROOT_URL}/add-user`,
                {
                    firebaseId:id,
                    email:email,
                    firstname:family_name,
                    lastname:given_name,
                    fromFirebase:1
                }).then((response)=>{
                    if(response.data.message == 'NEW_USER'){
                        dispatch({
                            type: 'AUTH_SUCCESS',
                            user: firebaseUser.additionalUserInfo.profile,
                            user_id:response.data.user_id,
                            isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                            provider:'google'
                        })
                    }
                }).catch((error)=>{
                    console.log(error)
                })
            }
            else{
                const {id} = firebaseUser.additionalUserInfo.profile
                dispatch(checkQuestionierData(id,1))
                dispatch(getHealthRecords(id,1))
                dispatch(fetchUserHealthRec(id,1))
                dispatch({
                    type: 'AUTH_SUCCESS',
                    user: firebaseUser.additionalUserInfo.profile,
                    user_id:null,
                    isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                    provider:'google'
                })
            }
        }
        catch (error) {
            //Alert.alert('Authentication Error', error.message)
            dispatch({ type: 'AUTH_ERROR', error: error.message })
        }
    }

}

export const signInWithEmail = (email, password)=>{
    return  (dispatch)=>{
        dispatch({ type: 'AUTH_START' })
        firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then((firebaseUser) => {
           const NewUser= firebaseUser.user._user
            const {email, displayName, uid } = NewUser
                const userNames = displayName.split(" ")
                let lastName = userNames[0]
                let firstName = userNames[1]
                const userData={
                    first_name:firstName,
                    last_name:lastName,
                    displayName:displayName,
                    email:email,
                    id:uid,
                    provider:'email'
                }
                dispatch(checkQuestionierData(uid,1))
                dispatch(getHealthRecords(uid,1))
                dispatch(fetchUserHealthRec(uid,1))
                dispatch({
                type: 'AUTH_SUCCESS',
                user: userData,
                user_id:null,
                isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                provider:'email'
            })
        })
        .catch(error => {
            console.log(error)
            dispatch({ type: 'AUTH_ERROR', error: error.message })
        })
    }
}


export const signUpWithEmail = (state)=>{
    const {email,password,lastName, firstName} = state
    const displayName = `${firstName} ${lastName}`    
    return  (dispatch)=>{
    dispatch({ type: 'EMAIL_REG_START'})
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((firebaseUser)=>{
        const newUser= firebaseUser.user._user
        const {email, uid } = newUser
        const userData={
            first_name:firstName,
            last_name:lastName,
            displayName:displayName,
            email:email,
            id:uid,
            provider:'firebase'
        }
        const isNew = firebaseUser.additionalUserInfo.isNewUser
        dispatch({ type: 'EMAIL_REG_SUCCESS'})
          firebaseUser.user.updateProfile({
            displayName: displayName
          })
          if (isNew === true){
            dispatch({ type: 'AUTH_START' })
            httpClient.post(`${ROOT_URL}/add-user`,
            {
                firebaseId:uid,
                email:email,
                firstname:firstName,
                lastname:lastName,
                fromFirebase:1
            }).then((response)=>{
                if(response.data.message == 'NEW_USER'){
                    dispatch({
                        type: 'AUTH_SUCCESS',
                        user: userData,
                        user_id:response.data.user_id,
                        isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                        provider:'email'
                    })
                }
                else{
                    dispatch({ type: 'AUTH_ERROR', error: error.message })
                }
            }).catch((error)=>{
                dispatch({ type: 'AUTH_ERROR', error: response.data.message})
            })
        }
        else{
            dispatch(checkQuestionierData(uid,1))
            dispatch(getHealthRecords(uid,1))
            dispatch(fetchUserHealthRec(uid,1))
            dispatch({
                type: 'AUTH_SUCCESS',
                user: userData,
                user_id:null,
                isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                provider:'email'
            }) 
        }
      })
      .catch(error => {
          console.log(error.message)
        dispatch({ type: 'EMAIL_REG_ERROR', error: error.message })
      })
    }
}



export const signInWithFacebook = () => {
    return async (dispatch) => {
        try {
            
            await LoginManager.logInWithPermissions([
                'public_profile',
                'email',
            ]);

            const data = await AccessToken.getCurrentAccessToken();
            if (!data) {
                dispatch({ type: 'AUTH_ERROR', error: 'No access Token recieved from facebook' })
            }

            // create  a new firebase credential with the token
            const credential = firebase.auth.FacebookAuthProvider.credential(
                data.accessToken,
            );
            // login with credential
            const firebaseUser = await firebase
                .auth()
                .signInWithCredential(credential);
                if (firebaseUser.additionalUserInfo.isNewUser == true){
                    const {first_name, last_name,middle_name,id} = firebaseUser.additionalUserInfo.profile
                    httpClient.post(`${ROOT_URL}/add-user`,
                    {
                        firebaseId:id,
                        email:null,
                        firstname:first_name,
                        lastname:`${last_name} ${middle_name}`,
                        fromFirebase:1
                    }).then((response)=>{
                        if(response.data.message == 'NEW_USER'){
                            dispatch({
                                type: 'AUTH_SUCCESS',
                                user: firebaseUser.additionalUserInfo.profile,
                                user_id:response.data.user_id,
                                isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                                provider:'google'
                            })
                        }
                    }).catch((error)=>{
                        console.log(error)
                        dispatch({ type: 'AUTH_ERROR', error: error.message })
                    })
                }
                else{
                    const {id} = firebaseUser.additionalUserInfo.profile
                    dispatch(checkQuestionierData(id,1))
                    dispatch(getHealthRecords(id,1))
                    dispatch(fetchUserHealthRec(id,1))
                    dispatch({
                        type: 'AUTH_SUCCESS',
                        user: firebaseUser.additionalUserInfo.profile,
                        user_id:null,
                        isNewUser: firebaseUser.additionalUserInfo.isNewUser,
                        provider:'google'
                    })
                }
            
        } catch (e) {
            dispatch({ type: 'AUTH_ERROR', error: e.message })
        }
    }
}


export const resetEmailSignup = ()=>{
    return(dispatch)=>{
        dispatch({type:'EMAIL_REG_RESET'})
    }
}

export const resetAuthState = ()=>{
    return(dispatch)=>{
        dispatch({type:'AUTH_RESET'})
    }
}