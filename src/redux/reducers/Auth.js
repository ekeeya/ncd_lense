     const initialState = {
        isLoggedIn: false,
        isLoading:false,
        error :null,
        user:null,
        user_id:null,
        isNewUser:null,
        provider:null,
    }


export default function signInWithFirebase(state=initialState, action){
    switch(action.type){
        case 'AUTH_START':
            return Object.assign({}, state,{
                isLoading:true
            })

        case 'AUTH_SUCCESS':
            return Object.assign({}, state,{
                isLoading:false,
                isLoggedIn:true,
                user:action.user,
                user_id:action.user_id,
                isNewUser:action.isNewUser,
                provider:action.provider
            })

        case 'AUTH_ERROR':
            return Object.assign({}, state,{
                error:action.error,
                isLoading:false,
            })
            case 'AUTH_RESET':
                return Object.assign({}, state,{
                    isLoggedIn: false,
                    isLoading:false,
                    error :null,
                    user:null,
                    user_id:null,
                    isNewUser:null,
                    provider:null,
                })

        default:
            return state
    }
}