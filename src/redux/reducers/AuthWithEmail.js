const initialState = {
    isRegistering:false,
    hasRegistered:false,
    regError:null
}


export default function signUpWithEmail(state=initialState, action){
switch(action.type){
    case 'EMAIL_REG_START':
        return Object.assign({}, state,{
            isRegistering:true
        })

    case 'EMAIL_REG_SUCCESS':
        return Object.assign({}, state,{
            isRegistering:false,
            hasRegistered:true,
        })

    case 'EMAIL_REG_ERROR':
        return Object.assign({}, state,{
            regError:action.error,
            isRegistering:false,
        })
        case 'EMAIL_REG_RESET':
            return Object.assign({}, state,{
                isRegistering:false,
                hasRegistered:false,
                regError:null
            })

    default:
        return state
}
}