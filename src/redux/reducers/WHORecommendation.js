const initialState = {
    isFetchingRec: false,
    whoRecommendation:null,
    fetchRecError:null
}


export default function getWHORecommendation(state=initialState, action){
switch(action.type){
    case 'WHO_START':
        return Object.assign({}, state,{
            isFetchingRec:true
        })

    case 'WHO_SUCCESS':
        return Object.assign({}, state,{
            isFetchingRec:false,
            whoRecommendation:action.recommendation,
            fetchRecError:null
        })

    case 'WHO_ERROR':
        return Object.assign({}, state,{
            isFetchingRec: false,
            fetchRecError:action.error
        })

    default:
        return state
}
}