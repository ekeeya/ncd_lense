const initialState = {
    working:false,
    rateError:null,
    succesRate:null,

}


export default function addFeedback(state=initialState, action){
switch(action.type){
    case 'START_RATE':
        return Object.assign({}, state,{
            working:true,
        })

    case 'RATE_SUCCESS':
        return Object.assign({}, state,{
            working:false,
            succesRate:true,
        })

    case 'RATE_ERROR':
        return Object.assign({}, state,{
            working:false,
            rateError:action.error,
            succesRate:false
        })

        case 'RESET_RATING':
        return Object.assign({}, state,{
            working:false,
            rateError:null,
            succesRate:null,
        })

    default:
        return state
}
}