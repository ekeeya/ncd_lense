const initialState = {
    adding:false,
    submitted:false,
    submitError:null,
    newRec:null
}


export default function addNewHealthRec(state=initialState, action){
switch(action.type){
    case 'ADD_START':
        return Object.assign({}, state,{
            adding:true
        })

    case 'ADD_SUCCESS':
        return Object.assign({}, state,{
            adding:false,
            submitted:true,
            newRec:action.record
        })

    case 'ADD_ERROR':
        return Object.assign({}, state,{
            submitError:action.error,
            adding:false
        })
        case 'ADD_RESET':
            return Object.assign({}, state,{
                adding:false,
                submitted:false,
                submitError:null,
            })

    default:
        return state
}
}