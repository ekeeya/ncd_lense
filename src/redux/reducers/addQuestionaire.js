const initialState = {
    submitting:false,
    errorSubmitting:null,
    doneSubmitting:null,

}


export default function addQuestionaire(state=initialState, action){
switch(action.type){
    case 'ADD_QUESTIONAIRE':
        return Object.assign({}, state,{
            submitting:true,
            doneSubmitting:false
        })

    case 'ADD_QUESTIONAIRE_SUCCESS':
        return Object.assign({}, state,{
            submitting:false,
            doneSubmitting:true,
        })

    case 'ADD_QUESTIONAIRE_ERROR':
        return Object.assign({}, state,{
            submitting:false,
            errorSubmitting:action.error,
            doneSubmitting:null
        })

        case 'RESET_QUESTIONAIRE_STATE':
        return Object.assign({}, state,{
            submitting:false,
            errorSubmitting:null,
            doneSubmitting:null
        })

    default:
        return state
}
}