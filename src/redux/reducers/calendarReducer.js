import actionTypes from '../actions/actionTypes'

const defaultState = {
    events: [],
    markedDates:{},
    isLoading: false,
    eventRemoved:null,
    eventRemoveError:null,
  };

 /* 
  function itemsLoaded(events) {
    return {
      type: ITEMS_LOADED,
      events,
    };
  } */

export default function CalendarReducer(state = defaultState, action) {
    switch (action.type) {
        case 'EVENTS_FETCH_START':
            return Object.assign({}, state, {
              isLoading: true,
            });
      case 'EVENTS_FETCHED':
        return Object.assign({}, state, {
          isLoading: false,
          events: action.events,
          markedDates:action.markedDates
        });
        case 'EVENT_REMOVE_START':
        return Object.assign({}, state, {
          isLoading: true,
        });
        case 'EVENT_REMOVE_SUCCESS':
        return Object.assign({}, state, {
          isLoading: false,
          eventRemoved:true
        });
        case 'EVENT_REMOVE_FAIL':
        return Object.assign({}, state, {
          isLoading: false,
          eventRemoved:false,
          eventRemoveError:action.error
        });
        case 'HIDE_DIALOG':
        return Object.assign({}, state, {
          eventRemoveError:null,
          eventRemoved:false
        });
      default:
        return state;
    }
  }