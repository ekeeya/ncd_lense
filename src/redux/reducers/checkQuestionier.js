const initialState = {
    checking: false,
    hasQuestionierData:null,
    bioInfo:null,
    checkError:null
}


export default function checkQuestionier(state=initialState, action){
switch(action.type){
    case 'CHECK_START':
        return Object.assign({}, state,{
            checking:true
        })

    case 'CHECK_SUCCESS':
        return Object.assign({}, state,{
            checking:false,
            hasQuestionierData:action.val,
            bioInfo:action.data
        })

    case 'CHECK_ERROR':
        return Object.assign({}, state,{
            checking: false,
            bioInfo:null,
            checkError:action.error
        })
        case 'CHECK_RESET':
            return Object.assign({}, state,{
                hasQuestionierData:null
            })

    default:
        return state
}
}