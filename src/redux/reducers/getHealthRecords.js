const initialState = {
    healthRecs:[],
    fetchingRec:false,
    fetchRecError:null

}


export default function fetchHealthRecords(state=initialState, action){
switch(action.type){
    case 'START_REC_CHECK':
        return Object.assign({}, state,{
            fetchingRec:true,
        })

    case 'REC_CHECK_SUCCESS':
        return Object.assign({}, state,{
            healthRecs:action.records,
            fetchingRec:false,
        })

    case 'REC_CHECK_ERROR':
        return Object.assign({}, state,{
            fetchingRec:false,
            fetchRecError:action.error
        })

    default:
        return state
}
}