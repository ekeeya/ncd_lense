const initialState = {
    ncdFacts:null,
    ncdGetError:null
}


export default function getNCDFacts(state=initialState, action){
switch(action.type){
    
    case 'GOT_NCD_FACTS':
        return Object.assign({}, state,{
            ncdFacts:action.facts,
        })

    case 'GOT_NCD_ERROR':
        return Object.assign({}, state,{
            ncdGetError:action.error
        })

    default:
        return state
}
}