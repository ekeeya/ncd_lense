const initialState = {
    isLoading: false,
    ncdData: [],
    ncdError: null
}


export default function getNCDs(state = initialState, action) {
    switch (action.type) {
        case 'GET_NCD_START':
            return Object.assign({}, state, {
                isLoading: true
            })

        case 'GET_NCD_SUCCESS':
            return Object.assign({}, state, {
                isLoading: false,
                ncdData: action.data,
            })

        case 'GET_NCD_ERROR':
            return Object.assign({}, state, {
                ncdError: action.error,
                isLoading: false,
            })


        default:
            return state
    }
}