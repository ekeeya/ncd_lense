const initialState = {
    healthError :null,
    healthRec:[],
    fetching:false
}


export default function UserhealthRecord(state=initialState, action){
switch(action.type){
    case 'HEALTH_FETCH_START':
        return Object.assign({}, state,{
            fetching:true
        })

    case 'HEALTH_FETCH_SUCCESS':
        return Object.assign({}, state,{
            fetching:false,
            healthRec:action.record
        })

    case 'HEALTH_FETCH_ERROR':
        return Object.assign({}, state,{
            healthError:action.error,
            fetching:false,
        })
        case 'HEALTH_FETCH_RESET':
            return Object.assign({}, state,{
                healthError :null,
                fetching:false
            })

    default:
        return state
}
}