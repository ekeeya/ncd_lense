const initialState = {
    isFetching:false,
    recError:null,
    recommendation:[]
}


export default function getRecommendation(state=initialState, action){
switch(action.type){
    case 'REC_START':
        return Object.assign({}, state,{
            isFetching:true
        })

    case 'REC_SUCCESS':
        return Object.assign({}, state,{
            isFetching:false,
            recommendation:action.rec
        })

    case 'REC_ERROR':
        return Object.assign({}, state,{
            isFetching:false,
            recError:action.error,
            recommendation:[],
        })

    default:
        return state
}
}