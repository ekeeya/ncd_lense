import { combineReducers } from 'redux';

// ## Generator Reducer Imports
import app from '../../modules/AppState';
import chat from '../../modules/chat/ChatState';
import signInWithFirebase from './Auth'
import CalendarReducer from './calendarReducer';
import UserhealthRecord from './latestHealthRecord';
import addNewHealthRec from './addHealthRec';
import getRecommendation from './recommendations'
import checkQuestionier from './checkQuestionier';
import addQuestionaire from './addQuestionaire';
import fetchHealthRecords from './getHealthRecords'
import addFeedback from './addFeedback';
import signUpWithEmail from './AuthWithEmail';
import getWHORecommendation from './WHORecommendation';
import getNCDs from './getNCDs';
import getNCDFacts from './getNCDFacts';

export default combineReducers({
  // ## Generator Reducers
  signInWithFirebase,
  UserhealthRecord,
  addNewHealthRec,
  getRecommendation,
  checkQuestionier,
  addQuestionaire,
  fetchHealthRecords,
  getNCDFacts,
  addFeedback,
  signUpWithEmail,
  app,
  getNCDs,
  CalendarReducer,
  chat,
  getWHORecommendation,
});
